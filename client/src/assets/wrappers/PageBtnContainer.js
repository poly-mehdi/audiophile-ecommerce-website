import styled from 'styled-components'

const Wrapper = styled.section`
  height: 6rem;
  margin-top: 2rem;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  gap: 1rem;
  width: 90vw;
  max-width: var(--max-width);
  margin: 0 auto;

  @media (min-width: 768px) {
    justify-content: end;
  }

  .btn-container {
    background: var(--black-primary);
    border-radius: 5px;
    display: flex;
  }
  .page-btn {
    background: transparent;
    border-color: transparent;
    width: 50px;
    height: 40px;
    font-weight: 700;
    font-size: 1.25rem;
    color: var(--orange-primary);
    border-radius: 2px;
    cursor: pointer;
  }
  .active {
    background: var(--orange-primary);
    color: var(--white-tertiary);
  }
  .prev-btn,
  .next-btn {
    background: var(--black-primary);
    border-color: transparent;
    border-radius: 2px;

    width: 100px;
    height: 40px;
    color: var(--orange-primary);
    text-transform: capitalize;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 0.5rem;
    font-size: 1rem;
    cursor: pointer;
  }
  .prev-btn:hover,
  .next-btn:hover {
    color: var(--orange-secondary);
  }
  .dots {
    display: grid;
    place-items: center;
    cursor: text;
  }
`

export default Wrapper
