import { NavLink } from 'react-router-dom'
import links from '../utils/Links'
import { useHomeContext } from '../pages/HomeLayout'

const NavLinks = () => {
  const { showCart, closeCart } = useHomeContext()
  const { user } = useHomeContext()

  return (
    <>
      {links.map((link) => {
        const { id, url, text } = link
        if (url === 'orders' && !user) return null
        return (
          <li key={id}>
            <NavLink
              to={url}
              onClick={showCart ? closeCart : null}
              className={({ isActive }) =>
                isActive ? 'nav-link active' : 'nav-link'
              }
            >
              {text}
            </NavLink>
          </li>
        )
      })}
    </>
  )
}
export default NavLinks
