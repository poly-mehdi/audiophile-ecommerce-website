import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import {
  HomeLayout,
  Landing,
  Error,
  Headphones,
  Earphones,
  Speakers,
  Register,
  Login,
  Checkout,
  Orders,
  SingleProduct,
} from './pages'
import { store } from './store'

// actions
import { action as registerAction } from './pages/Register'
import { action as loginAction } from './pages/Login'
import { action as checkoutAction } from './pages/Checkout'

// loaders
import { loader as homeLoader } from './pages/HomeLayout'
import { loader as singleProductLoader } from './pages/SingleProduct'
import { loader as headphonesLoader } from './pages/Headphones'
import { loader as speakersLoader } from './pages/Speakers'
import { loader as earphonesLoader } from './pages/Earphones'
import { loader as ordersLoader } from './pages/Orders'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 1000 * 60 * 5,
    },
  },
})

const router = createBrowserRouter([
  {
    path: '/',
    element: <HomeLayout />,
    errorElement: <Error />,
    loader: homeLoader,
    children: [
      {
        index: true,
        element: <Landing />,
      },
      {
        path: 'headphones',
        element: <Headphones />,
        loader: headphonesLoader,
      },
      {
        path: '/headphones/:id',
        element: <SingleProduct />,
        loader: singleProductLoader,
      },
      {
        path: 'earphones',
        element: <Earphones />,
        loader: earphonesLoader,
      },
      {
        path: '/earphones/:id',
        element: <SingleProduct />,
        loader: singleProductLoader,
      },
      {
        path: 'speakers',
        element: <Speakers />,
        loader: speakersLoader,
      },
      {
        path: '/speakers/:id',
        element: <SingleProduct />,
        loader: singleProductLoader,
      },
      {
        path: 'checkout',
        element: <Checkout />,
        action: checkoutAction(store),
      },
      {
        path: 'orders',
        element: <Orders />,
        loader: ordersLoader,
      },
    ],
  },
  {
    path: '/login',
    element: <Login />,
    errorElement: <Error />,
    action: loginAction,
  },
  {
    path: '/register',
    element: <Register />,
    errorElement: <Error />,
    action: registerAction,
  },
])

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  )
}
export default App
