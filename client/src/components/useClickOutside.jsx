import { useEffect } from 'react'

const useClickOutside = (ref, callback, excludedRef) => {
  const handleClick = (e) => {
    const target = e.target

    if (!target) return

    if (ref.current && !ref.current.contains(target)) {
      callback()
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleClick)

    return () => {
      document.removeEventListener('click', handleClick)
    }
  })
}

export default useClickOutside
