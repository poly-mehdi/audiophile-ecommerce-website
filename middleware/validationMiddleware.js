import mongoose from 'mongoose'
import { body, param, validationResult } from 'express-validator'
import { BadRequestError, NotFoundError } from '../errors/customErrors.js'
import Product from '../models/ProductModel.js'
import User from '../models/UserModel.js'

const withValidationErrors = (validateValues) => {
  return [
    validateValues,
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        const errorMessages = errors.array().map((error) => error.msg)
        if (errorMessages[0].startsWith('no products')) {
          throw new NotFoundError(errorMessages)
        }
        throw new BadRequestError(errorMessages)
      }
      next()
    },
  ]
}

export const validateOrderInput = withValidationErrors([
  body('name').notEmpty().withMessage('name is required'),
  body('email').notEmpty().withMessage('email is required'),
  body('phone').notEmpty().withMessage('phone is required'),
  body('address').notEmpty().withMessage('address is required'),
  body('zip').notEmpty().withMessage('zip is required'),
  body('city').notEmpty().withMessage('city is required'),
  body('country').notEmpty().withMessage('country is required'),
  body('payment').notEmpty().withMessage('payment is required'),
  body('productsAmount').notEmpty().withMessage('productsAmount is required'),
  body('total').notEmpty().withMessage('total is required'),
  // body('createdBy').notEmpty().withMessage('createdBy is required'),
])

export const validateIdParam = withValidationErrors([
  param('id').custom(async (value) => {
    const isValidId = mongoose.Types.ObjectId.isValid(value)
    if (!isValidId) throw new BadRequestError('invalid MongoDB id')
    const product = await Product.findById(value)
    if (!product) throw new NotFoundError(`no job with id : ${value}`)
  }),
])

export const validateCategory = withValidationErrors([
  param('category').custom(async (value) => {
    const products = await Product.find({ category: value })
    if (!products || products.length == 0)
      throw new NotFoundError(`no products found in ${value}`)
  }),
])

export const validateRegisterInput = withValidationErrors([
  body('name').notEmpty().withMessage('name is required'),
  body('lastName').notEmpty().withMessage('lastName is required'),
  body('email')
    .notEmpty()
    .withMessage('email is required')
    .isEmail()
    .withMessage('invalid email format')
    .custom(async (email) => {
      const user = await User.findOne({ email })
      if (user) {
        throw new BadRequestError('email already exists')
      }
    }),
  body('password')
    .notEmpty()
    .withMessage('password is required')
    .isLength({ min: 8 })
    .withMessage('password must be at least 8 characters long')
    .matches(/^(?=.*[a-z])(?=.*[A-Z])/)
    .withMessage(
      'password must contain at least one lowercase letter and one uppercase letter'
    ),
])

export const validateLoginInput = withValidationErrors([
  body('email')
    .notEmpty()
    .withMessage('email is required')
    .isEmail()
    .withMessage('invalid email format'),

  body('password').notEmpty().withMessage('password is required'),
])

export const validateUpdateUserInput = withValidationErrors([
  body('name').notEmpty().withMessage('name is required'),
  body('lastName').notEmpty().withMessage('lastName is required'),
  body('email')
    .notEmpty()
    .withMessage('email is required')
    .isEmail()
    .withMessage('invalid email format')
    .custom(async (email, { req }) => {
      const user = await User.findOne({ email })
      if (user && user._id.toString() !== req.user.userId) {
        throw new BadRequestError('email already exists')
      }
    }),
])
