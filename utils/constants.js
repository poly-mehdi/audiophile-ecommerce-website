export const PAYMENT_METHODS = { EMONEY: 'e-Money', CASH: 'Cash on Delivery' }
export const CATEGORIES = {
  HEADPHONES: 'headphones',
  EARPHONES: 'earphones',
  SPEAKERS: 'speakers',
}
