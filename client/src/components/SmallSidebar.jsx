import Wrapper from '../assets/wrappers/SmallSidebar'
import { useHomeContext } from '../pages/HomeLayout'
import { NavLink } from 'react-router-dom'

const SmallSidebar = () => {
  const { showSidebar, toggleSidebar, user } = useHomeContext()

  return (
    <Wrapper>
      <div
        className={
          showSidebar ? 'sidebar-container show-sidebar' : 'sidebar-container'
        }
      >
        <div className='nav-links'>
          <NavLink
            to='/headphones'
            onClick={toggleSidebar}
            className='nav-link'
          >
            <p className='nav-link-text'>headphones</p>
            <span>{'>'}</span>
          </NavLink>
          <NavLink to='/speakers' onClick={toggleSidebar} className='nav-link'>
            <p className='nav-link-text'>speakers</p>
            <span>{'>'}</span>
          </NavLink>
          <NavLink to='/earphones' onClick={toggleSidebar} className='nav-link'>
            <p className='nav-link-text'>earphones</p>
            <span>{'>'}</span>
          </NavLink>
          {user && (
            <NavLink to='/orders' onClick={toggleSidebar} className='nav-link'>
              <p className='nav-link-text'>my orders</p>
              <span>{'>'}</span>
            </NavLink>
          )}
        </div>
      </div>
    </Wrapper>
  )
}

export default SmallSidebar
