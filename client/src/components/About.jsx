import Wrapper from '../assets/wrappers/About'

const About = () => {
  return (
    <Wrapper>
      <picture>
        <source
          srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/shared/desktop/image-best-gear.jpg'
          media='(min-width: 992px)'
        />
        <source
          srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/shared/tablet/image-best-gear.jpg'
          media='(min-width: 768px)'
        />
        <img
          src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/shared/mobile/image-best-gear.jpg'
          alt='about-img'
          className='img'
        />
      </picture>
      <div className='about-text'>
        <h2>
          Bringing you the <span>best</span> audio gear
        </h2>
        <p>
          Located at the heart of New York City, Audiophile is the premier store
          for high end headphones, earphones, speakers, and audio accessories.
          We have a large showroom and luxury demonstration rooms available for
          you to browse and experience a wide range of our products. Our trained
          staff are more than happy to help you with any questions you may have.
        </p>
      </div>
    </Wrapper>
  )
}
export default About
