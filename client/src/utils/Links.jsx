import { IoHomeOutline } from 'react-icons/io5'
import { BsHeadphones, BsCart3 } from 'react-icons/bs'
import { MdOutlineShoppingCartCheckout } from 'react-icons/md'
import { TbShoppingCartSearch } from 'react-icons/tb'

const links = [
  { id: 1, url: '/', text: 'home', icon: <IoHomeOutline /> },
  { id: 2, url: 'headphones', text: 'headphones', icon: <BsHeadphones /> },
  { id: 3, url: 'speakers', text: 'speakers', icon: <BsHeadphones /> },
  { id: 4, url: 'earphones', text: 'earphones', icon: <BsHeadphones /> },
  // { id: 5, url: 'cart', text: 'cart', icon: <BsCart3 /> },
  // {
  //   id: 6,
  //   url: 'checkout',
  //   text: 'checkout',
  //   icon: <MdOutlineShoppingCartCheckout />,
  // },
  { id: 5, url: 'orders', text: 'orders', icon: <TbShoppingCartSearch /> },
]

export default links
