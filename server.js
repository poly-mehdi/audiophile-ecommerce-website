import * as dotenv from 'dotenv'
import 'express-async-errors'
dotenv.config()
import express from 'express'
const app = express()
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
// database
import mongoose from 'mongoose'

// routers
import productRouter from './routes/productRouter.js'
import orderRouter from './routes/orderRouter.js'
import authRouter from './routes/authRouter.js'
import userRouter from './routes/userRouter.js'

// public
import { dirname } from 'path'
import { fileURLToPath } from 'url'
import path from 'path'

// middleware
import errorHandlerMiddleware from './middleware/error-handler.js'
import {
  authenticateUser,
  softAuthenticateUser,
} from './middleware/authMiddleware.js'

const __dirname = dirname(fileURLToPath(import.meta.url))

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}
app.use(express.static(path.join(__dirname, './client/dist')))
app.use(cookieParser())
app.use(express.json())

app.get('/', (req, res) => {
  res.send('Hello World')
})

app.use('/api/v1/products', productRouter)
app.use('/api/v1/orders', authenticateUser, orderRouter)
app.use('/api/v1/users', softAuthenticateUser, userRouter)
app.use('/api/v1/auth', authRouter)

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, './client/dist', 'index.html'))
})

app.use('*', (req, res) => {
  res.status(404).json({ msg: 'not found' })
})

app.use(errorHandlerMiddleware)

const port = process.env.PORT || 5100

try {
  await mongoose.connect(process.env.MONGO_URI)
  app.listen(port, () => {
    console.log(`server running on PORT ${port}....`)
  })
} catch (error) {
  console.log(error)
  process.exit(1)
}
