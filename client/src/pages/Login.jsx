import Wrapper from '../assets/wrappers/LoginAndRegisterPage'
import FormRow from '../components/FormRow'
import {
  Form,
  Link,
  redirect,
  useNavigation,
  useNavigate,
} from 'react-router-dom'
import customFetch from '../utils/customFetch'
import { toast } from 'react-toastify'

export const action = async ({ request }) => {
  const formData = await request.formData()
  const data = Object.fromEntries(formData)
  try {
    await customFetch.post('/auth/login', data)
    toast.success('Login successful')
    return redirect('/')
  } catch (error) {
    toast.error(error?.response?.data?.msg)
    return error
  }
}

const Login = () => {
  const navigation = useNavigation()
  const isSubmitting = navigation.state === 'submitting'

  const navigate = useNavigate()

  const loginDemoUser = async () => {
    const data = {
      email: 'test@test.com',
      password: 'secret123',
    }

    try {
      await customFetch.post('/auth/login', data)
      toast.success('Take a test drive')
      navigate('/')
    } catch (error) {
      toast.error(error?.response?.data?.msg)
    }
  }
  return (
    <Wrapper>
      <Form method='post' className='form'>
        <h4>Login</h4>
        <FormRow
          type='email'
          name='email'
          placeholder='johndoe@gmail.com'
          autoComplete='email'
        />
        <FormRow
          type='password'
          name='password'
          placeholder='········'
          autoComplete='current-password'
        />
        <button type='submit' className='btn-submit' disabled={isSubmitting}>
          {isSubmitting ? 'submitting...' : 'submit'}
        </button>
        <button type='button' className='btn-guest' onClick={loginDemoUser}>
          guest user
        </button>
        <p>
          Not a member yet?
          <Link to='/register'>Register</Link>
        </p>
      </Form>
    </Wrapper>
  )
}
export default Login
