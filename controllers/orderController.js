import Order from '../models/OrderModel.js'
import { StatusCodes } from 'http-status-codes'
import { BadRequestError } from '../errors/customErrors.js'

export const getAllOrders = async (req, res) => {
  const totalOrders = await Order.countDocuments({ createdBy: req.user.userId })

  const page = Number(req.query.page) || 1
  const limit = Number(req.query.limit) || 10
  const skip = (page - 1) * limit
  const numOfPages = Math.ceil(totalOrders / limit)

  const orders = await Order.find({ createdBy: req.user.userId })
    .sort({
      createdAt: -1,
    })
    .skip(skip)
    .limit(limit)
  res
    .status(StatusCodes.OK)
    .json({ totalOrders, orders, currentPage: page, numOfPages })
}

export const createOrder = async (req, res) => {
  const {
    name,
    email,
    phone,
    address,
    zip,
    city,
    country,
    payment,
    productsAmount,
    total,
  } = req.body
  req.body.createdBy = req.user.userId

  const order = await Order.create(req.body)
  res.status(StatusCodes.CREATED).json({ order })
}
