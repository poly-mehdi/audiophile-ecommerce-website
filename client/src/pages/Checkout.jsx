import Wrapper from '../assets/wrappers/CheckoutPage'
import { FormRow } from '../components'
import {
  Form,
  Link,
  useNavigation,
  useNavigate,
  redirect,
  useActionData,
} from 'react-router-dom'
import { CartItemCheckout } from '../components'
import { formatPrice } from '../utils/format'
import { useSelector } from 'react-redux'
import { BsCartXFill } from 'react-icons/bs'
import { useHomeContext } from './HomeLayout'
import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { clearCart } from '../features/cart/cartSlice'
import customFetch from '../utils/customFetch'
import { PAYMENT_METHODS } from '../../../utils/constants'

const scrollToTopPage = () => {
  window.scrollTo({
    top: 0,
    behavior: 'smooth',
  })
}

export const action =
  (store) =>
  async ({ request }) => {
    const formData = await request.formData()
    const { name, email, phone, address, zip, city, country, payment } =
      Object.fromEntries(formData)

    const { orderTotal, numItemsInCart } = store.getState().cartState

    const info = {
      name,
      email,
      phone,
      address,
      zip,
      city,
      country,
      payment,
      productsAmount: numItemsInCart,
      total: orderTotal,
    }

    const errors = { msg: '' }
    if (name.length > 30) {
      errors.msg = 'name is too long'
      return errors
    }
    if (email.length > 100) {
      errors.msg = 'email is too long'
      return errors
    }
    if (phone.length > 14) {
      errors.msg = 'phone is too long'
      return errors
    }
    if (address.length > 100) {
      errors.msg = 'address is too long'
      return errors
    }
    if (zip.length > 6) {
      errors.msg = 'zip is too long'
      return errors
    }
    if (city.length > 30) {
      errors.msg = 'city is too long'
      return errors
    }
    if (country.length > 30) {
      errors.msg = 'country is too long'
      return errors
    }
    if (
      payment !== PAYMENT_METHODS.EMONEY &&
      payment !== PAYMENT_METHODS.CASH
    ) {
      errors.msg = 'invalid payment method'
      return errors
    }

    try {
      const response = await customFetch.post('/orders', info)
      store.dispatch(clearCart())
      toast.success('order placed successfully')
      scrollToTopPage()
      // return redirect('/orders')
      return null
    } catch (error) {
      errors.msg = error?.response?.data?.msg
      if (error.response.status === 401) {
        return redirect('/login')
      }
      return errors
    }
  }

const Checkout = () => {
  const navigation = useNavigation()
  const isSubmitting = navigation.state === 'submitting'
  const [paymentMethod, setPaymentMethod] = useState('')

  const { shipping, tax, cartTotal, orderTotal, numItemsInCart } = useSelector(
    (state) => state.cartState
  )
  const cartItems = useSelector((state) => state.cartState.cartItems)

  const { user, openOrderValidation } = useHomeContext()
  const navigate = useNavigate()

  const errors = useActionData()

  useEffect(() => {
    if (!user) {
      toast.warn('You must be logged in to checkout.')
      navigate('/login')
    }
  }, [])

  const handlePaymentMethodChange = (event) => {
    setPaymentMethod(event.target.value)
  }

  const handleSubmit = (event) => {
    const form = event.target
    const formData = new FormData(form)

    if (
      formData.get('name').length > 30 ||
      formData.get('email').length > 100 ||
      formData.get('phone').length > 14 ||
      formData.get('address').length > 100 ||
      formData.get('zip').length > 6 ||
      formData.get('city').length > 30 ||
      formData.get('country') > 30 ||
      (formData.get('payment') !== PAYMENT_METHODS.EMONEY &&
        formData.get('payment') !== PAYMENT_METHODS.CASH)
    ) {
      return
    } else {
      openOrderValidation()
    }
  }

  return (
    <Wrapper>
      <Form method='POST' className='form' onSubmit={handleSubmit}>
        <div className='form-container'>
          <h3>Checkout</h3>
          {errors && <p className='error-container'>Warning : {errors.msg}</p>}
          <div className='billing-details-container'>
            <h6>billing details</h6>
            <div className='billing-details'>
              <FormRow type='text' name='name' />
              <FormRow type='email' name='email' labelText='email address' />
              <FormRow
                type='tel'
                name='phone'
                labelText='phone number'
                pattern='^\+1\s\d{3}-\d{3}-\d{4}$'
              />
            </div>
          </div>
          <div className='shipping-info-container'>
            <h6>shipping info</h6>
            <FormRow type='text' name='address' />
            <div className='shipping-info'>
              <FormRow type='text' name='zip' />
              <FormRow type='text' name='city' />
              <FormRow type='text' name='country' />
            </div>
          </div>
          <div className='payment-details-container'>
            <h6>payment details</h6>
            <div className='payment-details'>
              <label htmlFor='paymentDetails' className='form-label-payment'>
                payment method
              </label>
              <div className='radio-container'>
                <div className='radio'>
                  <input
                    type='radio'
                    id='paymentMethod1'
                    name='payment'
                    className='form-input-radio'
                    value={PAYMENT_METHODS.EMONEY}
                    checked={paymentMethod === PAYMENT_METHODS.EMONEY}
                    onChange={handlePaymentMethodChange}
                    required
                  />
                  <label htmlFor='paymentMethod1' className='form-label-radio'>
                    e-Money
                  </label>
                </div>
                <div className='radio'>
                  <input
                    type='radio'
                    id='paymentMethod2'
                    name='payment'
                    className='form-input-radio'
                    value={PAYMENT_METHODS.CASH}
                    checked={paymentMethod === PAYMENT_METHODS.CASH}
                    onChange={handlePaymentMethodChange}
                    required
                  />
                  <label
                    htmlFor='paymentMethod2'
                    className='form-label-radio cash'
                  >
                    Cash on Delivery
                  </label>
                </div>
              </div>
            </div>
          </div>
          {/* <div className='payment-method'>
            <FormRow
              type='text'
              name='eMoneyNumber'
              labelText='e-Money Number'
            />
            <FormRow type='text' name='eMoneyPin' labelText='e-Money PIN' />
          </div> */}
          {paymentMethod === PAYMENT_METHODS.EMONEY && (
            <div className='payment-method'>
              <FormRow
                type='text'
                name='eMoneyNumber'
                labelText='e-Money Number'
              />
              <FormRow type='text' name='eMoneyPin' labelText='e-Money PIN' />
            </div>
          )}
        </div>
        {numItemsInCart !== 0 ? (
          <div className='cart-container'>
            <h6>Summary</h6>
            <div className='cart-items'>
              {cartItems.map((item) => {
                return <CartItemCheckout key={item.cartID} cartItem={item} />
              })}
            </div>
            <div className='cart-resume'>
              <div className='amount'>
                <h6>total</h6>
                <p>{formatPrice(cartTotal)}</p>
              </div>
              <div className='amount'>
                <h6>shipping</h6>
                <p>{formatPrice(shipping)}</p>
              </div>
              <div className='amount'>
                <h6>vat (included)</h6>
                <p>{formatPrice(tax)}</p>
              </div>
              <div className='amount'>
                <h6>grand total</h6>
                <p>{formatPrice(orderTotal)}</p>
              </div>
            </div>
            <button type='submit' className='btn btn-submit'>
              {isSubmitting ? 'submitting...' : 'continue & pay'}
            </button>
          </div>
        ) : (
          <div className='cart-container-empty'>
            <h5 className='empty-text'>Your cart is empty</h5>
            <BsCartXFill className='empty-icon' size={30} />
          </div>
        )}
      </Form>
    </Wrapper>
  )
}
export default Checkout
