import styled from 'styled-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;

  /* * {
    border: 1px solid red;
  } */

  .landing-hero-container {
    background-color: var(--black-tertiary);

    .landing-hero-items {
      width: 90vw;
      margin: 0 auto;
      max-width: var(--max-width);

      @media (min-width: 992px) {
        width: 85vw;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding-right: 3rem;
      }

      .landing-hero {
        display: flex;
        flex-direction: column;
        text-align: center;
        gap: 1rem;
        padding: 9rem 0rem;
        background-image: url('https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/mobile/image-header.jpg');
        background-size: contain;
        background-position: center bottom;
        background-repeat: no-repeat;

        @media (min-width: 768px) {
          background-image: url('https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/tablet/image-header.jpg');
        }

        @media (min-width: 992px) {
          background-image: none;
        }

        p:first-of-type {
          text-transform: uppercase;
          color: var(--grey-secondary);
          letter-spacing: 4px;
          width: 300px;
          margin: 0 auto;
          justify-self: flex-start;

          @media (min-width: 992px) {
            margin: 0;
            text-align: left;
            font-size: 1.2rem;
          }
        }

        h2 {
          line-height: 1;
          color: var(--white-primary);
          max-width: 300px;
          margin: 0 auto;

          @media (min-width: 992px) {
            max-width: 450px;
            font-size: 3.5rem;
            text-align: left;
          }
        }

        p:last-of-type {
          color: var(--grey-primary);
          max-width: 300px;
          margin: 0 auto;

          @media (min-width: 992px) {
            max-width: 450px;
            text-align: left;
            font-size: 1rem;
          }
        }

        .btn-hero {
          background-color: var(--orange-primary);
          color: var(--white-primary);
          margin: 0 auto;
          padding: 15px 20px;
          width: 200px;
          font-size: 15px;
          font-weight: 700;
          text-transform: uppercase;
          cursor: pointer;

          &:hover {
            background-color: var(--orange-secondary);
          }
          @media (min-width: 992px) {
            margin: 0;
          }
        }
      }

      .landing-hero-image {
        display: none;

        @media (min-width: 992px) {
          display: block;
          width: auto;
          height: 40rem;
        }
      }
    }
  }

  .shop-container {
    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);
    padding: 2rem 0;
    margin-top: 6rem;

    @media (min-width: 992px) {
      width: 85vw;
    }

    .shop-items {
      display: flex;
      flex-direction: column;
      text-align: center;
      gap: 6rem;

      @media (min-width: 768px) {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        gap: 5px;
      }

      .shop-item {
        background-color: var(--white-primary);
        border-radius: 1rem;
        height: 10rem;
        position: relative;
        display: flex;
        align-items: end;
        justify-content: center;

        @media (min-width: 768px) {
          width: calc(90vw / 3 - 5px * 2);
        }

        @media (min-width: 992px) {
          width: calc(85vw / 3 - 5px * 2);
        }

        @media (min-width: 1250px) {
          width: calc(1120px / 3 - 5px * 2);
        }

        .shop-image {
          width: 11rem;
          margin: 0 auto;
          position: absolute;
          left: 50%;
          transform: translate(-50%, -40%);
        }

        .shop-item-text {
          display: flex;
          flex-direction: column;
          align-items: center;
          gap: 0.5rem;
          padding-bottom: 1rem;

          h6 {
            text-transform: capitalize;
          }

          .btn-container {
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;

            .btn-shop {
              background-color: transparent;
              color: var(--grey-secondary);
              text-transform: uppercase;
              display: inline-block;
              padding: 0.5rem 0.5rem;
              font-size: 13px;

              &:hover {
                color: var(--orange-primary);
              }
            }
          }
        }
      }
    }
  }

  .landing-products {
    display: flex;
    flex-direction: column;
    gap: 2rem;
    padding: 2rem 0;
    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);

    @media (min-width: 992px) {
      width: 85vw;
    }

    .landing-product {
      display: flex;
      flex-direction: column;
      text-align: center;
      gap: 2rem;
      border-radius: 1rem;
      background-color: var(--orange-primary);
      padding: 4rem 1rem;

      @media (min-width: 992px) {
        display: grid;
        grid-template-columns: 1fr 1fr;
        gap: 2rem;
        padding: 2rem 2rem;
      }

      picture {
        width: 10rem;
        margin: 0 auto;

        @media (min-width: 768px) {
          width: 15rem;
        }

        /* @media (min-width: 992px) {
          width: 67%;
        } */

        /* ::before,
        ::after {
          content: '';
          position: absolute;
          border-radius: 50%;
          border: 0.5px solid var(--grey-primary);
          z-index: -1;
          left: 50%;
          transform: translateX(-50%);
        }

        ::before {
          width: 70vw;
          height: 70vw;
          top: -25%;
        }

        ::after {
          width: 60vw;
          height: 60vw;
          top: -15%;
        } */
      }

      .landing-product-text {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 2rem;

        @media (min-width: 992px) {
          align-items: flex-start;
          padding: 2rem 2rem;
        }

        .landing-product-text-title {
          h3 {
            text-transform: uppercase;
            color: var(--white-primary);

            @media (min-width: 992px) {
              text-align: left;
              font-size: 3.5rem;
              line-height: 1;
            }
          }
        }

        p {
          color: var(--grey-primary);
          @media (min-width: 992px) {
            text-align: left;
            padding-right: 2rem;
          }
        }
        .btn-landing-1 {
          background-color: var(--black-tertiary);
          padding: 1rem 2rem;
          color: var(--white-primary);
          font-size: 15px;
          letter-spacing: 1px;
          text-transform: uppercase;
          cursor: pointer;

          &:hover {
            background-color: var(--grey-secondary);
          }
        }
      }
    }

    .landing-product-speaker {
      position: relative;

      picture {
        img {
          border-radius: 1rem;
        }
      }
      .landing-product-text {
        position: absolute;
        top: 50%;
        left: 8%;
        transform: translateY(-50%);
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 2rem;

        h3 {
          letter-spacing: 1px;
        }

        .btn-landing-2 {
          border: 1px solid var(--black-primary);
          padding: 1rem 2rem;
          font-size: 15px;
          letter-spacing: 1px;
          font-weight: bold;
          color: var(--black-primary);
          background-color: transparent;
          text-transform: uppercase;
          cursor: pointer;

          &:hover {
            color: var(--white-primary);
            background-color: var(--black-primary);
          }
        }
      }
    }

    .landing-product-earphone {
      display: flex;
      flex-direction: column;

      @media (min-width: 768px) {
        flex-direction: row;
        align-items: center;
      }

      picture {
        img {
          border-radius: 1rem;
        }
      }

      .landing-product-text {
        display: flex;
        flex-direction: column;
        align-items: start;
        gap: 2rem;
        padding: 2rem 1rem;

        @media (min-width: 992px) {
          padding: 2rem 6rem;
        }

        h3 {
          letter-spacing: 1px;
        }

        .btn-landing-3 {
          border: 1px solid var(--black-primary);
          padding: 1rem 2rem;
          font-size: 15px;
          letter-spacing: 1px;
          font-weight: bold;
          color: var(--black-primary);
          background-color: transparent;
          text-transform: uppercase;
          cursor: pointer;

          &:hover {
            color: var(--white-primary);
            background-color: var(--black-primary);
          }
        }
      }
    }
  }
`

export default Wrapper
