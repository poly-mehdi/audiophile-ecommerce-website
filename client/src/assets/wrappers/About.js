import styled from 'styled-components'

const Wrapper = styled.section`
  width: 90vw;
  margin: 0 auto;
  max-width: var(--max-width);
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 2rem 0;
  gap: 2rem;

  @media (min-width: 768px) {
    gap: 3rem;
  }

  @media (min-width: 992px) {
    gap: 4rem;
    width: 85vw;
    display: grid;
    grid-template-columns: 1fr 1fr;
  }

  picture {
    @media (min-width: 992px) {
      order: 2;
    }
  }
  .about-text {
    @media (min-width: 992px) {
      order: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      gap: 2rem;
    }

    h2 {
      letter-spacing: 0.1rem;
      line-height: 1;
      text-align: center;
      span {
        color: var(--orange-primary);
      }
      @media (min-width: 992px) {
        text-justify: auto;
      }
    }

    p {
      text-align: center;
      @media (min-width: 992px) {
        text-align: justify;
      }
    }
  }
`

export default Wrapper
