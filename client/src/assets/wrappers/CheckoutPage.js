import styled from 'styled-components'

const Wrapper = styled.section`
  width: 90vw;
  margin: 0 auto;
  max-width: var(--max-width);

  /* * {
    border: 1px solid red;
  } */

  .form {
    display: flex;
    flex-direction: column;
    gap: 2rem;

    @media (min-width: 992px) {
      flex-direction: row;
    }

    .form-container {
      display: flex;
      flex-direction: column;
      margin-top: 2rem;
      gap: 1rem;
      border-radius: 5px;
      background-color: var(--white-tertiary);
      padding: 1rem;

      @media (min-width: 992px) {
        flex: 2;
      }

      h6 {
        color: var(--orange-primary);
      }

      .error-container {
        color: red;
        border: 1px solid red;
        border-radius: 5px;
        padding: 0.5rem;
      }

      .form-row {
        display: flex;
        flex-direction: column;
        gap: 0.25rem;

        .form-label {
          font-weight: bold;
          text-transform: capitalize;
        }
      }

      .form-label-payment {
        font-weight: bold;
        text-transform: capitalize;
      }

      .form-input {
        padding: 1rem 1.5rem;
        border-radius: 5px;
        border: 1px solid var(--grey-primary);
        font-size: 1rem;
        transition: var(--transition);
        outline: none;

        &:focus {
          border-color: var(--orange-primary);
        }
      }

      .billing-details-container {
        display: flex;
        flex-direction: column;
        gap: 1rem;

        .billing-details {
          display: flex;
          flex-direction: column;
          gap: 0.75rem;

          @media (min-width: 768px) {
            display: grid;
            grid-template-columns: 1fr 1fr;
            gap: 1rem;
          }
        }
      }

      .shipping-info-container {
        display: flex;
        flex-direction: column;
        gap: 1rem;

        .shipping-info {
          display: flex;
          flex-direction: column;
          gap: 0.75rem;

          @media (min-width: 768px) {
            display: grid;
            grid-template-columns: 1fr 1fr;
            gap: 1rem;
          }
        }
      }

      .payment-details-container {
        display: flex;
        flex-direction: column;
        gap: 1rem;

        .payment-details {
          display: flex;
          flex-direction: column;
          gap: 0.75rem;

          @media (min-width: 768px) {
            display: grid;
            grid-template-columns: 1fr 1fr;
            gap: 1rem;
          }

          .radio-container {
            display: flex;
            flex-direction: column;
            gap: 0.75rem;
          }
        }
      }

      .payment-method {
        display: flex;
        flex-direction: column;
        gap: 1rem;

        @media (min-width: 768px) {
          display: grid;
          grid-template-columns: 1fr 1fr;
          gap: 1rem;
        }
      }

      .radio {
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 1rem;
        padding: 1rem 1.5rem;
        font-size: 1rem;
        transition: var(--transition);
        outline: 1px solid var(--grey-primary);
        border-radius: 5px;

        .form-label-radio {
          font-weight: bold;

          &:checked:focus {
            background-color: red;
          }
        }

        .cash {
          text-transform: capitalize;
        }
      }
    }

    .cart-container {
      display: flex;
      flex-direction: column;
      gap: 2rem;
      background-color: var(--white-tertiary);
      padding: 1rem;
      height: fit-content;
      border-radius: 5px;

      @media (min-width: 992px) {
        flex: 1;
        margin-top: 2rem;
      }

      .cart-items {
        display: flex;
        flex-direction: column;
        gap: 1rem;
        width: 100%;

        .cart-item {
          display: flex;
          align-items: center;
          justify-content: space-between;

          .cart-item-col-1 {
            display: flex;
            align-items: center;
            gap: 1rem;

            .item-img {
              border-radius: 5px;
              width: 60px;
            }

            .cart-item-product {
              display: flex;
              flex-direction: column;

              h6 {
                font-size: 1rem;
              }

              p {
                color: var(--grey-secondary);
                font-weight: bold;
              }
            }
          }
          .cart-item-amount {
            display: flex;
            align-items: center;

            span {
              font-weight: bold;
              color: var(--grey-secondary);
            }
          }
        }
      }

      .cart-resume {
        display: flex;
        flex-direction: column;
        gap: 0.25rem;
        justify-content: center;

        .amount {
          display: flex;
          align-items: center;
          justify-content: space-between;

          h6 {
            font-size: 1rem;
            color: var(--grey-secondary);
            font-weight: 500;
          }

          p {
            font-size: 1rem;
            font-weight: bold;
          }
        }

        .amount:last-child {
          margin-top: 1rem;

          p {
            color: var(--orange-primary);
          }
        }
      }

      .btn-submit {
        width: 100%;
        padding: 1rem 0;
        background-color: var(--orange-primary);
        color: var(--white-tertiary);
        font-weight: bold;
        font-size: 1rem;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: var(--transition);

        &:hover {
          background-color: var(--orange-secondary);
        }
      }
    }

    .cart-container-empty {
      display: flex;
      flex-direction: column;
      align-items: center;
      gap: 2rem;
      height: fit-content;
      background-color: var(--white-tertiary);
      padding: 1rem;
      border-radius: 5px;

      @media (min-width: 992px) {
        flex: 1;
        margin-top: 2rem;
      }

      .empty-icon {
        fill: var(--orange-secondary);
      }
    }
  }
`

export default Wrapper
