import styled from 'styled-components'

const Wrapper = styled.section`
  min-height: 100vh;
  display: grid;
  align-items: center;

  .form {
    width: 90vw;
    max-width: 400px;
    margin: 3rem auto;
    padding: 2rem 2.5rem;
    background-color: var(--white-tertiary);
    border-radius: var(--border-radius);
    box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1),
      0 2px 4px -1px rgba(0, 0, 0, 0.06);
    border-top: 7px solid var(--orange-primary);

    h4 {
      text-transform: capitalize;
      margin-bottom: 1rem;
      text-align: center;
    }

    .form-label {
      display: block;
      text-transform: capitalize;
      line-height: 1.5;
      font-weight: 400;
    }

    .form-input {
      width: 100%;
      padding: 0.375rem 0.75rem;
      border-radius: var(--border-radius);
      background: transparent;
      border: 1px solid var(--grey-primary);
      color: var(--black-primary);
      height: 2.5rem;
      font-size: 18px;
      cursor: pointer;
      margin-bottom: 1rem;

      &:focus {
        outline: none;
        border-color: var(--orange-primary);
      }
    }

    .btn-submit,
    .btn-guest {
      cursor: pointer;
      border: transparent;
      border-radius: var(--border-radius);
      padding: 0.375rem 0.75rem;
      transition: var(--transition);
      text-transform: capitalize;
      display: block;
      margin-top: 1rem;
      width: 100%;
      height: 2.5rem;
      font-size: 14px;
    }

    .btn-submit {
      background-color: var(--orange-primary);
      color: var(--white-primary);

      &:hover {
        background-color: var(--orange-secondary);
      }
    }

    .btn-guest {
      background-color: var(--white-tertiary);
      color: var(--black-secondary);
      border: 1px solid var(--black-primary);

      &:hover {
        background-color: var(--grey-secondary);
      }
    }

    p {
      margin-top: 1rem;
      text-align: center;

      a {
        color: var(--orange-primary);
        text-decoration: underline;
        margin-left: 0.5rem;
      }
    }
  }
`

export default Wrapper
