import styled from 'styled-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;

  /* * {
    border: 1px solid red;
  } */

  .speaker-title-container {
    background-color: var(--black-tertiary);
    padding: 3rem 0;
    text-align: center;
    gap: 1rem;

    @media (min-width: 768px) {
      padding: 4rem 0;
    }

    @media (min-width: 992px) {
      padding: 5rem 0;
    }
    h2 {
      color: var(--white-primary);
    }
  }

  .speakers {
    padding: 2rem 0;
    margin-bottom: 2rem;
    display: flex;
    flex-direction: column;
    gap: 4rem;
    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);

    @media (min-width: 992px) {
      width: 85vw;
    }

    .speaker {
      display: flex;
      flex-direction: column;
      gap: 2rem;
      text-align: center;

      @media (min-width: 992px) {
        display: grid;
        grid-template-columns: 1fr 1fr;
        gap: 2rem;
        align-items: center;
        text-align: left;

        picture {
          img {
            width: 100%;
            margin: 0;
            border-radius: 10px;
          }
        }
      }

      .speaker-text {
        display: flex;
        flex-direction: column;
        gap: 1rem;

        @media (min-width: 768px) {
          padding: 0 1rem;
        }

        @media (min-width: 992px) {
          padding: 0;
        }
      }

      .btn-speaker {
        background-color: var(--orange-primary);
        color: white;
        margin: 0 auto;
        text-align: center;
        padding: 15px 20px;
        width: 200px;
        font-size: 15px;
        font-weight: 700;
        text-transform: uppercase;
        cursor: pointer;

        &:hover {
          background-color: var(--orange-secondary);
        }

        @media (min-width: 992px) {
          margin: 0;
        }
      }
    }

    .second-product {
      @media (min-width: 992px) {
        picture {
          order: 2;
        }
        .headphone-text {
          order: 1;
        }
      }
    }
  }

  .shop-container {
    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);
    padding: 2rem 0;
    margin-top: 6rem;

    @media (min-width: 992px) {
      width: 85vw;
    }

    .shop-items {
      display: flex;
      flex-direction: column;
      text-align: center;
      gap: 6rem;

      @media (min-width: 768px) {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        gap: 5px;
      }

      .shop-item {
        background-color: var(--white-primary);
        border-radius: 1rem;
        height: 10rem;
        position: relative;
        display: flex;
        align-items: end;
        justify-content: center;

        @media (min-width: 768px) {
          width: calc(90vw / 3 - 5px * 2);
        }

        @media (min-width: 992px) {
          width: calc(85vw / 3 - 5px * 2);
        }

        @media (min-width: 1250px) {
          width: calc(1120px / 3 - 5px * 2);
        }

        .shop-image {
          width: 11rem;
          margin: 0 auto;
          position: absolute;
          left: 50%;
          transform: translate(-50%, -40%);
        }

        .shop-item-text {
          display: flex;
          flex-direction: column;
          align-items: center;
          gap: 0.5rem;
          padding-bottom: 1rem;

          h6 {
            text-transform: capitalize;
          }

          .btn-container {
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;

            .btn-shop {
              background-color: transparent;
              color: var(--grey-secondary);
              text-transform: uppercase;
              display: inline-block;
              padding: 0.5rem 0.5rem;
              font-size: 13px;

              &:hover {
                color: var(--orange-primary);
              }
            }
          }
        }
      }
    }
  }
`

export default Wrapper
