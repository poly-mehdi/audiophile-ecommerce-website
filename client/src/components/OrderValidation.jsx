import Wrapper from '../assets/wrappers/OrderValidation'
import confirmation from '../assets/checkout/icon-order-confirmation.svg'
import { useSelector } from 'react-redux'
import { formatPrice } from '../utils/format'
import CartItem from './CartItem'
import { Link } from 'react-router-dom'
import { useHomeContext } from '../pages/HomeLayout'

const OrderValidation = () => {
  const cartFirstItem = useSelector(
    (state) => state.cartState.overview?.firstItem
  )
  const orderTotal = useSelector((state) => state.cartState.overview?.total)
  const amount = useSelector((state) => state.cartState.overview?.amount)
  const nameFirstItem = cartFirstItem
    ? cartFirstItem.name?.split(' ').slice(0, -1).join(' ')
    : ''

  const { showOrderValidation, closeOrderValidation } = useHomeContext()

  return (
    <Wrapper>
      <div
        className={
          showOrderValidation ? 'order-container show-order' : 'order-container'
        }
      >
        {orderTotal > 0 ? (
          <div className='content'>
            <img
              src={confirmation}
              alt='confirmation'
              className='logo'
              height={70}
            />
            <div className='order-title'>
              <h5>thank you for your order</h5>
              <p>You will receive an email confirmation shortly.</p>
              <p style={{ fontStyle: 'italic' }}>
                If you are connected with test user Zippy, the database will not
                be updated.
              </p>
            </div>
            <div className='order-preview'>
              <div className='order-items'>
                <div className='order-items-row-1'>
                  <div className='order-info'>
                    <img
                      className='item-logo img'
                      src={cartFirstItem.image?.mobile}
                    ></img>

                    <div className='item-content'>
                      <h6>{nameFirstItem}</h6>
                      <p>{formatPrice(cartFirstItem.price)}</p>
                    </div>
                  </div>
                  <span>x{cartFirstItem.amount}</span>
                </div>
                {amount > 1 ? (
                  <p className='others'>
                    and {amount} other item
                    {amount > 1 ? 's' : ''}
                  </p>
                ) : (
                  ''
                )}
              </div>
              <div className='order-total'>
                <h6>grand total</h6>
                <p>{formatPrice(orderTotal)}</p>
              </div>
            </div>
            <Link
              to='/orders'
              className='btn order-btn'
              onClick={closeOrderValidation}
            >
              see your orders
            </Link>
          </div>
        ) : (
          ''
        )}
      </div>
    </Wrapper>
  )
}
export default OrderValidation
