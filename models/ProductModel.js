import mongoose from 'mongoose'

const { Schema } = mongoose

const imageSchema = new Schema(
  {
    mobile: String,
    tablet: String,
    desktop: String,
  },
  { _id: false }
)

const includesSchema = new Schema(
  {
    quantity: Number,
    item: String,
  },
  { _id: false }
)

const gallerySchema = new Schema(
  {
    first: imageSchema,
    second: imageSchema,
    third: imageSchema,
  },
  { _id: false }
)

const ProductSchema = new Schema({
  slug: String,
  name: String,
  image: imageSchema,
  category: String,
  categoryImage: imageSchema,
  new: Boolean,
  price: Number,
  description: String,
  features: String,
  includes: [includesSchema],
  gallery: gallerySchema,
  others: [
    {
      slug: String,
      name: String,
      image: imageSchema,
    },
  ],
})

export default mongoose.model('Product', ProductSchema)
