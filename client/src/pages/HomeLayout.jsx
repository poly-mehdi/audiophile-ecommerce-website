import {
  Outlet,
  redirect,
  useLoaderData,
  useNavigate,
  useLocation,
} from 'react-router-dom'
import {
  Footer,
  Header,
  Navbar,
  SmallSidebar,
  Cart,
  OrderValidation,
} from '../components'
import { useState, createContext, useContext } from 'react'
import customFetch from '../utils/customFetch'
import { toast } from 'react-toastify'
import Wrapper from '../assets/wrappers/HomeLayout'
import ScrollToTop from '../components/ScrollToTop'

export const loader = async () => {
  try {
    const { data } = await customFetch.get('/users/current-user')
    return data
  } catch (error) {
    return redirect('/error')
  }
}

const HomeContext = createContext()

const HomeLayout = () => {
  const { userWithoutPassword: user } = useLoaderData()
  const location = useLocation()
  const navigate = useNavigate()

  const [showSidebar, setShowSidebar] = useState(false)
  const [showCart, setShowCart] = useState(false)
  const [showOrderValidation, setShowOrderValidation] = useState(false)

  const openOrderValidation = () => {
    setShowOrderValidation(true)
  }

  const closeOrderValidation = () => {
    setShowOrderValidation(false)
  }

  const closeSidebar = () => {
    setShowSidebar(false)
    document.body.style.overflow = 'auto'
  }

  const closeCart = () => {
    setShowCart(false)
    document.body.style.overflow = 'auto'
  }

  const toggleSidebar = () => {
    showCart ? closeCart() : ''
    setShowSidebar(!showSidebar)
    showSidebar
      ? (document.body.style.overflow = 'auto')
      : (document.body.style.overflow = 'hidden')
  }

  const toggleCart = () => {
    showSidebar ? closeSidebar() : ''
    setShowCart(!showCart)
    showCart
      ? (document.body.style.overflow = 'auto')
      : (document.body.style.overflow = 'hidden')
  }

  const logoutUser = async () => {
    navigate('/')
    await customFetch.get('/auth/logout')
    navigate('/')
    toast.success('Logged out successfully')
  }

  return (
    <HomeContext.Provider
      value={{
        user,
        showSidebar,
        toggleSidebar,
        logoutUser,
        showCart,
        toggleCart,
        closeCart,
        showOrderValidation,
        openOrderValidation,
        closeOrderValidation,
      }}
    >
      <Wrapper>
        <ScrollToTop />
        <main className='home'>
          <SmallSidebar />
          <Cart />
          <OrderValidation />
          <div>
            <Header />
            <Navbar />
            <div
              className={
                location.pathname === '/checkout'
                  ? 'home-page checkout-page'
                  : 'home-page'
              }
            >
              <Outlet context={{ user }} />
            </div>
            <Footer />
          </div>
        </main>
      </Wrapper>
    </HomeContext.Provider>
  )
}

export const useHomeContext = () => useContext(HomeContext)

export default HomeLayout
