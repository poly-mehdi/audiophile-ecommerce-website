import { StatusCodes } from 'http-status-codes'
import User from '../models/UserModel.js'
import Order from '../models/OrderModel.js'

export const getCurrentUser = async (req, res) => {
  if (!req.user) {
    res.status(StatusCodes.OK).json({ userWithoutPassword: null })
    return
  }
  const user = await User.findOne({ _id: req.user.userId })
  const userWithoutPassword = user.toJSON()
  res.status(StatusCodes.OK).json({ userWithoutPassword })
}

export const getApplicationStats = async (req, res) => {
  const users = await User.countDocuments()
  const orders = await Order.countDocuments()
  res.status(StatusCodes.OK).json({ users, orders })
}

export const updateUser = async (req, res) => {
  const obj = { ...req }
  delete obj.password
  const updatedUser = await User.findByIdAndUpdate(req.user.userId, obj)
  res.status(StatusCodes.OK).json({ msg: 'user updated' })
}
