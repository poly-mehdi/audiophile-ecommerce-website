import { Router } from 'express'
import { getAllOrders, createOrder } from '../controllers/orderController.js'
import { validateOrderInput } from '../middleware/validationMiddleware.js'
import { checkForTestUser } from '../middleware/authMiddleware.js'
const router = Router()

// get all orders and create a new order
router
  .route('/')
  .get(getAllOrders)
  .post(checkForTestUser, validateOrderInput, createOrder)

export default router
