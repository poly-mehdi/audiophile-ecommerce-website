// db/connect.js après la correction
import mongoose from 'mongoose'

const connectDB = async (uri) => {
  await mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
}

export default connectDB
