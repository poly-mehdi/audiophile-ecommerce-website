import Wrapper from '../assets/wrappers/SingleProductPage'
import customFetch from '../utils/customFetch'
import { Link, useLoaderData } from 'react-router-dom'
import { formatPrice, formatTextForDisplay } from '../utils/format'
import { ShopMenu } from '../components'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addItem } from '../features/cart/cartSlice'

export const loader = async ({ params }) => {
  const response = await customFetch(`/products/${params.id}`)
  return { product: response.data.product }
}

const SingleProduct = () => {
  const { product } = useLoaderData()
  const {
    category,
    _id: id,
    description,
    features,
    categoryImage,
    gallery,
    image,
    includes,
    name,
    new: isNew,
    others,
    price,
  } = product

  const [amount, setAmount] = useState(1)

  const addAmount = () => {
    if (amount < 5) setAmount(amount + 1)
  }

  const subtractAmount = () => {
    if (amount > 1) {
      setAmount(amount - 1)
    }
  }

  const cartProduct = {
    cartID: id,
    image: categoryImage,
    name,
    price,
    amount,
  }

  const dispatch = useDispatch()

  const addToCart = () => {
    dispatch(addItem({ product: cartProduct }))
  }

  return (
    <Wrapper>
      <div className='navigation'>
        <ul>
          <li>
            <Link to='/' className='navigation-link'>
              Home
            </Link>
          </li>
          <li className='navigation-link'>{`>`}</li>
          <li>
            <Link to={`/${category}`} className='navigation-link'>
              {category}
            </Link>
          </li>
        </ul>
      </div>
      <div className='product-header'>
        <picture>
          <source media='(min-width: 992px)' srcSet={image.desktop} />
          <source media='(min-width: 768px)' srcSet={image.tabletSingle} />
          <img src={image.mobile} alt='speaker-1' className='img-product' />
        </picture>
        <div className='product-text'>
          {isNew && <p className='overline'>new product</p>}
          <h3>{name}</h3>
          <p>{description}</p>
          <h4>{formatPrice(price)}</h4>
          <div className='cart'>
            <div className='amount'>
              <span className='amount-add ' onClick={subtractAmount}>
                -
              </span>
              <span>{amount}</span>
              <span className='amount-add' onClick={addAmount}>
                +
              </span>
            </div>
            <button className='btn btn-product' onClick={addToCart}>
              add to cart
            </button>
          </div>
        </div>
      </div>
      <div className='product-information'>
        <div className='features'>
          <h4>features</h4>
          <p>{formatTextForDisplay(features)}</p>
        </div>
        <div className='box'>
          <h4>in the box</h4>
          <ul className='box-items'>
            {includes.map((include, index) => (
              <li className='box-item' key={index}>
                <span className='quantity'>{include.quantity}x</span>
                <p className='item-name'>{include.item}</p>
              </li>
            ))}
          </ul>
        </div>
      </div>

      <div className='gallery'>
        <div className='gallery-col-1'>
          <picture>
            <source media='(min-width: 992px)' srcSet={gallery.first.desktop} />
            <source media='(min-width: 768px)' srcSet={gallery.first.tablet} />
            <img src={gallery.first.mobile} alt='gallery-1' className='img' />
          </picture>
          <picture>
            <source
              media='(min-width: 992px)'
              srcSet={gallery.second.desktop}
            />
            <source media='(min-width: 768px)' srcSet={gallery.second.tablet} />
            <img src={gallery.second.mobile} alt='gallery-2' className='img' />
          </picture>
        </div>
        <picture>
          <source media='(min-width: 992px)' srcSet={gallery.third.desktop} />
          <source media='(min-width: 768px)' srcSet={gallery.third.tablet} />
          <img src={gallery.third.mobile} alt='gallery-3' className='img' />
        </picture>
      </div>
      <div className='others'>
        <h4>you may also like</h4>
        <div className='others-container'>
          {others.map((other, index) => (
            <div className='other' key={index}>
              <picture>
                <source
                  media='(min-width: 992px)'
                  srcSet={other.image.desktop}
                />
                <source
                  media='(min-width: 768px)'
                  srcSet={other.image.tablet}
                />
                <img
                  src={other.image.mobile}
                  alt='other'
                  className='img other-img'
                />
              </picture>
              <div className='other-text'>
                <h4>{other.name}</h4>
                <Link
                  to={`/${other.category}/${other._id}`}
                  className='btn btn-other'
                >
                  see product
                </Link>
              </div>
            </div>
          ))}
        </div>
      </div>
      <ShopMenu />
    </Wrapper>
  )
}
export default SingleProduct
