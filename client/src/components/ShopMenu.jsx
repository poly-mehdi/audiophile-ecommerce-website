import { BsChevronRight } from 'react-icons/bs'
import { Link } from 'react-router-dom'

const ShopMenu = () => {
  return (
    <section className='shop-container'>
      <div className='shop-items'>
        <div className='shop-item'>
          <img
            src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/shared/desktop/image-category-thumbnail-headphones.png'
            alt='shop-headphone'
            className='img shop-image'
          />
          <div className='shop-item-text'>
            <h6>Headphones</h6>
            <div className='btn-container'>
              <Link to='/headphones' className='btn btn-shop'>
                shop
              </Link>
              <BsChevronRight fill='#d87d4a' />
            </div>
          </div>
        </div>
        <div className='shop-item'>
          <img
            src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/shared/desktop/image-category-thumbnail-speakers.png'
            alt='shop-speaker'
            className='img shop-image'
          />
          <div className='shop-item-text'>
            <h6>Speakers</h6>
            <div className='btn-container'>
              <Link to='/speakers' className='btn btn-shop'>
                shop
              </Link>
              <BsChevronRight fill='#d87d4a' />
            </div>
          </div>
        </div>
        <div className='shop-item'>
          <img
            src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/shared/desktop/image-category-thumbnail-earphones.png'
            alt='shop-earphone'
            className='img shop-image'
          />
          <div className='shop-item-text'>
            <h6>Earphones</h6>
            <div className='btn-container'>
              <Link to='/earphones' className='btn btn-shop'>
                shop
              </Link>
              <BsChevronRight fill='#d87d4a' />
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
export default ShopMenu
