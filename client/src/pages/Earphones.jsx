import Wrapper from '../assets/wrappers/EarphonesPage'
import { ShopMenu, About } from '../components'
import { useLoaderData, Link } from 'react-router-dom'
import customFetch from '../utils/customFetch'

export const loader = async () => {
  const response = await customFetch(`/products/category/earphones`)
  return { products: response.data.products }
}

const Earphones = () => {
  const { products } = useLoaderData()
  const categoryAll = products[0].category
  return (
    <Wrapper>
      <div className='earphone-title-container'>
        <div className='earphone-title'>
          <h2>{categoryAll}</h2>
        </div>
      </div>
      {products.map((product, index) => {
        const {
          name,
          image,
          new: isNew,
          description,
          category,
          _id: id,
        } = product
        const isSecondProduct = index === 1
        return (
          <div className='earphones' key={index}>
            <div
              className={`earphone ${isSecondProduct ? 'second-product' : ''}`}
            >
              <picture>
                <source media='(min-width: 992px)' srcSet={image.desktop} />
                <source media='(min-width: 768px)' srcSet={image.tablet} />
                <img src={image.mobile} alt='earphone-1' className='img' />
              </picture>
              <div className='earphone-text'>
                {isNew && <p className='overline'>new product</p>}
                <div className='earphone-text-title'>
                  <h3>{name}</h3>
                  <h3>{category}</h3>
                </div>
                <p className='earphone-description'>{description}</p>
                <Link to={`/earphones/${id}`} className='btn-earphone'>
                  see product
                </Link>
              </div>
            </div>
          </div>
        )
      })}
      <ShopMenu />
      <About />
    </Wrapper>
  )
}
export default Earphones
