import styled from 'styled-components'

const Wrapper = styled.aside`
  /* * {
    border: 1px solid red;
  } */

  .order-container {
    position: fixed;
    top: calc(3.5rem + 1.5rem + 1px);
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.4);
    display: flex;
    justify-content: center;
    z-index: -1;
    opacity: 0;
    transition: var(--transition);

    .content {
      background: var(--white-tertiary);
      margin-top: 2rem;
      width: 90vw;
      height: fit-content;
      max-height: 70vh;
      border-radius: 10px;
      padding: 2.5rem 2rem;
      position: relative;
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      gap: 1.5rem;
      overflow: auto;

      @media (min-width: 768px) {
        width: 75vw;
      }

      @media (min-width: 992px) {
        width: 75vw;
        max-width: 40rem;
      }

      .order-title {
        h5 {
          margin-bottom: 1rem;
        }
        p {
          color: var(--grey-secondary);
          font-weight: 500;
        }
      }

      .order-preview {
        display: flex;
        flex-direction: column;
        width: 100%;

        @media (min-width: 768px) {
          flex-direction: row;
        }

        .order-items {
          background-color: var(--grey-primary);
          border-top-left-radius: 5px;
          border-top-right-radius: 5px;
          padding: 1.5rem 1.5rem;
          flex: 1;

          @media (min-width: 768px) {
            border-top-right-radius: 0;
            border-bottom-left-radius: 5px;
            border-top-left-radius: 5px;
            border-bottom-right-radius: 0;
          }

          .order-items-row-1 {
            display: flex;
            border-bottom: 0.5px solid var(--grey-secondary);
            align-items: center;
            justify-content: space-between;
            padding: 1rem 0;

            .order-info {
              display: flex;
              gap: 0.5rem;

              .item-logo {
                width: 3rem;
              }

              .item-content {
                h6 {
                  color: var(--black-primary);
                  font-weight: 700;
                  font-size: 1rem;
                }

                p {
                  color: var(--grey-secondary);
                  font-weight: 700;
                }
              }
            }

            span {
              color: var(--grey-secondary);
              font-weight: 700;
            }
          }

          .others {
            text-align: center;
            color: var(--grey-secondary);
          }
        }

        .order-total {
          background-color: var(--black-secondary);
          border-bottom-left-radius: 5px;
          border-bottom-right-radius: 5px;
          padding: 1.5rem 2rem;
          flex: 1;

          @media (min-width: 768px) {
            border-top-right-radius: 5px;
            border-bottom-left-radius: 0;
            border-top-left-radius: 0;
            border-bottom-right-radius: 5px;
            display: flex;
            flex-direction: column;
            justify-content: center;
          }

          h6 {
            color: var(--grey-secondary);
            font-weight: 500;
            margin-bottom: 0.5rem;
          }

          p {
            color: var(--white-primary);
            font-weight: 700;
            font-size: 1.25rem;
          }
        }
      }

      .order-btn {
        width: 100%;
        display: flex;
        justify-content: center;
        background-color: var(--orange-primary);
        color: var(--white-primary);
        padding: 1rem 0;
        font-size: 1rem;
        font-weight: 700;

        &:hover {
          background-color: var(--orange-secondary);
        }
      }
    }
  }

  .show-order {
    z-index: 99;
    opacity: 1;
  }
`
export default Wrapper
