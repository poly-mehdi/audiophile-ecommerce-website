import { useSelector } from 'react-redux'
import Wrapper from '../assets/wrappers/Cart'
import { useHomeContext } from '../pages/HomeLayout'
import { Link } from 'react-router-dom'
import CartItem from './CartItem'
import { formatPrice } from '../utils/format'
import { useDispatch } from 'react-redux'
import { clearCart } from '../features/cart/cartSlice'
import { BsCartXFill } from 'react-icons/bs'
import { RxCross2 } from 'react-icons/rx'

const Cart = () => {
  const { showCart, toggleCart, user } = useHomeContext()
  const dispatch = useDispatch()

  const { numItemsInCart, shipping, tax, cartTotal } = useSelector(
    (state) => state.cartState
  )
  const cartItems = useSelector((state) => state.cartState.cartItems)

  const clearCartItems = () => {
    dispatch(clearCart())
  }

  return (
    <Wrapper>
      <div className={showCart ? 'cart-container show-cart' : 'cart-container'}>
        {numItemsInCart > 0 ? (
          <div className='content'>
            <div className='cart-information'>
              <h4>Cart({numItemsInCart})</h4>
              <div className='cart-information-end'>
                <p onClick={clearCartItems}>remove all</p>
                <RxCross2
                  onClick={toggleCart}
                  className='close-icon'
                  size={30}
                />
              </div>
            </div>
            <div className='cart-items'>
              {cartItems.map((item) => {
                return <CartItem key={item.cartID} cartItem={item} />
              })}
            </div>
            <div className='amount'>
              <h6>total</h6>
              <p>{formatPrice(cartTotal)}</p>
            </div>
            {user && (
              <Link
                to='/checkout'
                onClick={toggleCart}
                className='btn-container'
              >
                checkout
              </Link>
            )}
            {!user && (
              <Link to='/login' onClick={toggleCart} className='btn-container'>
                Please login
              </Link>
            )}
          </div>
        ) : (
          <div className='content'>
            <h5 className='empty-text'>Your cart is empty</h5>
            <BsCartXFill className='empty-icon' size={30} />
          </div>
        )}
      </div>
    </Wrapper>
  )
}
export default Cart
