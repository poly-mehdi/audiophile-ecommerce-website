import { Form, redirect, useNavigation, Link } from 'react-router-dom'
import Wrapper from '../assets/wrappers/LoginAndRegisterPage'
import FormRow from '../components/FormRow'
import customFetch from '../utils/customFetch'
import { toast } from 'react-toastify'

export const action = async ({ request }) => {
  const formData = await request.formData()
  const data = Object.fromEntries(formData)

  try {
    await customFetch.post('/auth/register', data)
    toast.success('Registration successful')
    return redirect('/login')
  } catch (error) {
    toast.error(error?.response?.data?.msg || 'An error occurred')
    return error
  }
}

const Register = () => {
  const navigation = useNavigation()
  const isSubmitting = navigation.state === 'submitting'
  return (
    <Wrapper>
      <Form method='post' className='form'>
        <h4>Register</h4>
        <FormRow type='text' name='name' placeholder='John' />
        <FormRow
          type='text'
          name='lastName'
          labelText='last name'
          placeholder='Doe'
        />
        <FormRow type='email' name='email' placeholder='johndoe@gmail.com' />
        <FormRow type='password' name='password' placeholder='········' />
        <button type='submit' className='btn-submit' disabled={isSubmitting}>
          {isSubmitting ? 'submitting...' : 'submit'}
        </button>
        <p>
          Already a member ?<Link to='/login'>Login</Link>
        </p>
      </Form>
    </Wrapper>
  )
}
export default Register
