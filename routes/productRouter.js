import { Router } from 'express'
import {
  getAllProducts,
  getCategoryProducts,
  getProduct,
} from '../controllers/productController.js'
import {
  validateIdParam,
  validateCategory,
} from '../middleware/validationMiddleware.js'

const router = Router()

// get all products
router.route('/').get(getAllProducts)
router.route('/:id').get(validateIdParam, getProduct)
router.route('/category/:category').get(validateCategory, getCategoryProducts)

export default router
