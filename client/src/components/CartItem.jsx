import { editSubtractItem, editAddItem } from '../features/cart/cartSlice'
import { formatPrice } from '../utils/format'
import { useDispatch } from 'react-redux'

const CartItem = ({ cartItem }) => {
  const { cartID, image, name, price, amount } = cartItem
  const dispatch = useDispatch()

  const addItem = () => {
    dispatch(editAddItem({ cartID, amount }))
  }
  const subtractItem = () => {
    dispatch(editSubtractItem({ cartID, amount }))
  }
  return (
    <div className='cart-item'>
      <div className='cart-item-col-1'>
        <img src={image.mobile} alt='product' className='img item-img' />
        <div className='cart-item-product'>
          <h6>{name}</h6>
          <p>{formatPrice(price)}</p>
        </div>
      </div>
      <div className='cart-item-amount'>
        <span className='amount-add ' onClick={subtractItem}>
          -
        </span>
        <span>{amount}</span>
        <span className='amount-add' onClick={addItem}>
          +
        </span>
      </div>
    </div>
  )
}
export default CartItem
