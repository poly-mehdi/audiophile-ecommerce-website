import { useLoaderData, Link } from 'react-router-dom'
import customFetch from '../utils/customFetch'
import Wrapper from '../assets/wrappers/LandingPage'
import { About, ShopMenu } from '../components'

const Landing = () => {
  return (
    <Wrapper>
      <div className='landing-hero-container'>
        <div className='landing-hero-items'>
          <div className='landing-hero'>
            <p className='landing-hero-subtitle'>new product</p>
            <h2>XX99 mark ii headphones</h2>
            <p>
              Experience natural, lifelike audio and exceptional build quality
              made for the passionate music enthusiast.
            </p>
            <Link
              to='/headphones/65df791093118c2b0b3ec036'
              className='btn-hero'
            >
              see product
            </Link>
          </div>
          <img
            src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/mobile/image-header.jpg'
            alt='header-mobile'
            className='landing-hero-image'
          />
        </div>
      </div>
      <ShopMenu />
      <div className='landing-products'>
        <div className='landing-product'>
          <picture>
            <source
              srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/desktop/image-speaker-zx9.png'
              media='(min-width: 992px)'
            />
            <source
              srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/tablet/image-speaker-zx9.png'
              media='(min-width: 768px)'
            />
            <img
              src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/mobile/image-speaker-zx9.png'
              alt='speaker-1'
              className='img'
            />
          </picture>
          <div className='landing-product-text'>
            <div className='landing-product-text-title'>
              <h3>xz9</h3>
              <h3>speaker</h3>
            </div>
            <p>
              Upgrade to premium speakers that are phenomenally built to deliver
              truly remarkable sound.
            </p>
            <Link
              to='/speakers/65df791093118c2b0b3ec03e'
              className='btn-landing-1'
            >
              see product
            </Link>
          </div>
        </div>
        <div className='landing-product-speaker'>
          <picture>
            <source
              srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/desktop/image-speaker-zx7.jpg'
              media='(min-width: 992px)'
            />
            <source
              srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/tablet/image-speaker-zx7.jpg'
              media='(min-width: 768px)'
            />
            <img
              src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/mobile/image-speaker-zx7.jpg'
              alt='speaker-2'
              className='img'
            />
          </picture>
          <div className='landing-product-text'>
            <h3>xz7 speaker</h3>
            <Link
              to='/speakers/65df791093118c2b0b3ec03a'
              className='btn-landing-2'
            >
              see product
            </Link>
          </div>
        </div>
        <div className='landing-product-earphone'>
          <picture>
            <source
              srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/desktop/image-earphones-yx1.jpg'
              media='(min-width: 992px)'
            />
            <source
              srcSet='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/tablet/image-earphones-yx1.jpg'
              media='(min-width: 768px)'
            />
            <img
              src='https://audiophile-website.s3.us-east-2.amazonaws.com/assets/home/mobile/image-earphones-yx1.jpg'
              alt='earphones'
              className='img'
            />
          </picture>
          <div className='landing-product-text'>
            <h3>yz1 earphones</h3>
            <Link
              to='/earphones/65df791093118c2b0b3ec02a'
              className='btn-landing-3'
            >
              see product
            </Link>
          </div>
        </div>
      </div>
      <About />
    </Wrapper>
  )
}
export default Landing
