import Product from '../models/ProductModel.js'
import { StatusCodes } from 'http-status-codes'
import { NotFoundError } from '../errors/customErrors.js'

export const getAllProducts = async (req, res) => {
  const products = await Product.find({})
  res.status(StatusCodes.OK).send({ products })
}

export const getCategoryProducts = async (req, res) => {
  const { category } = req.params
  const products = await Product.find({ category: category }).sort({ _id: -1 })
  res.status(StatusCodes.OK).json({ products })
}

export const getProduct = async (req, res) => {
  const { id } = req.params
  const product = await Product.findById(id)
  res.status(StatusCodes.OK).json({ product })
}
