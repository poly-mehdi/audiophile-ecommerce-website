import mongoose from 'mongoose'
import { PAYMENT_METHODS } from '../utils/constants.js'

const OrderSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please provide name'],
      maxlength: 30,
    },
    email: {
      type: String,
      required: [true, 'Please provide email'],
      maxlength: 100,
    },
    phone: {
      type: String,
      required: [true, 'Please provide phone number'],
      maxlength: 14,
    },
    address: {
      type: String,
      required: [true, 'Please provide address'],
      maxlength: 100,
    },
    zip: {
      type: String,
      required: [true, 'Please provide zip code'],
      maxlength: 6,
    },
    city: {
      type: String,
      required: [true, 'Please provide city'],
      maxlength: 30,
    },
    country: {
      type: String,
      required: [true, 'Please provide country'],
      maxlength: 30,
    },
    payment: {
      type: String,
      enum: [PAYMENT_METHODS.EMONEY, PAYMENT_METHODS.CASH],
      default: PAYMENT_METHODS.EMONEY,
    },
    productsAmount: {
      type: Number,
      required: true,
    },
    total: {
      type: Number,
      required: true,
    },
    createdBy: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
    },
  },
  { timestamps: true }
)

export default mongoose.model('Order', OrderSchema)
