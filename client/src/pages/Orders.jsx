import Wrapper from '../assets/wrappers/OrdersPage'
import customFetch from '../utils/customFetch'
import { useLoaderData, useLocation } from 'react-router-dom'
import { formatDate, formatPrice } from '../utils/format'
import { BsChevronDoubleLeft, BsChevronDoubleRight } from 'react-icons/bs'
import { PageBtnContainer } from '../components'
import { createContext, useContext, useEffect } from 'react'
import { toast } from 'react-toastify'
import { useHomeContext } from './HomeLayout'

export const loader = async ({ request }) => {
  const params = Object.fromEntries([
    ...new URL(request.url).searchParams.entries(),
  ])

  try {
    const response = await customFetch(`/orders`, {
      params,
    })
    return response.data
  } catch (error) {
    toast.error(error?.response?.data?.msg)
    return error
  }
}
const AllOrdersContext = createContext()
const Orders = () => {
  const { orders, totalOrders, numOfPages, currentPage } = useLoaderData()
  const { openOrderValidation } = useHomeContext()

  return (
    <AllOrdersContext.Provider value={{ currentPage, numOfPages }}>
      <Wrapper>
        <div className='orders-title-container'>
          <div className='order-title'>
            <h2>My orders</h2>
          </div>
        </div>
        {orders.length !== 0 && (
          <>
            <div className='orders-container'>
              <h6>
                {totalOrders} order{orders.length > 1 && 's'} found
              </h6>
              <div className='order-title'>
                <p className='order-title-item'>name</p>
                <p className='order-title-item'>address</p>
                <p className='order-title-item'>products</p>
                <p className='order-title-item'>cost</p>
                <p className='order-title-item'>date</p>
              </div>
              <div className='orders-items'>
                {orders.map((order, index) => {
                  const {
                    name,
                    address,
                    productsAmount,
                    total,
                    createdAt: date,
                  } = order
                  return (
                    <div key={index} className='order-item'>
                      <p className='order-items-item'>{name}</p>
                      <p className='order-items-item'>{address}</p>
                      <p className='order-items-item'>{productsAmount}</p>
                      <p className='order-items-item'>{formatPrice(total)}</p>
                      <p className='order-items-item'>{formatDate(date)}</p>
                    </div>
                  )
                })}
              </div>
            </div>
            {numOfPages > 1 && <PageBtnContainer />}
          </>
        )}
        {orders.length === 0 && (
          <div className='orders-container-empty'>
            <h4>no orders yet</h4>
          </div>
        )}
      </Wrapper>
    </AllOrdersContext.Provider>
  )
}
export default Orders

export const useAllOrdersContext = () => useContext(AllOrdersContext)
