import styled from 'styled-components'

const Wrapper = styled.header`
  background-color: var(--orange-primary);

  .header-items {
    display: flex;
    justify-content: space-around;
    align-items: center;
    width: 90vw;
    margin: 0 auto;

    @media (min-width: 768px) {
      justify-content: flex-end;
    }

    @media (min-width: 992px) {
      width: 85vw;
      max-width: var(--max-width);
    }

    .header-links {
      display: flex;
      align-items: center;
      gap: 10px;
      height: 1.5rem;

      .header-link {
        color: var(--white-tertiary);
        font-size: 12px;
        cursor: pointer;
      }
    }

    .header-links-login {
      display: flex;
      align-items: center;
      gap: 10px;

      p {
        font-size: 16px;
        color: var(--white-tertiary);
        text-transform: capitalize;
      }

      .btn-logout {
        background-color: transparent;
        border: 1px solid var(--white-tertiary);
        border-radius: 2px;
        padding: 2px 5px;
        color: var(--white-tertiary);

        &:hover {
          background-color: var(--orange-secondary);
        }
      }
    }
  }
`

export default Wrapper
