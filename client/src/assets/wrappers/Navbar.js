import styled from 'styled-components'

const Wrapper = styled.nav`
  background-color: var(--black-tertiary);
  min-height: 3.6rem;
  border-bottom: 1px solid var(--grey-primary);

  /* * {
    border: 1px solid red;
  } */

  @media (min-width: 768px) {
    border-bottom: none;
  }

  @media (min-width: 992px) {
    border-bottom: none;
    min-height: 4rem;
  }

  .navbar {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin: 0 auto;
    padding: 1rem 0;
    width: 90vw;
    max-width: var(--max-width);

    @media (min-width: 768px) {
      border-bottom: 2px solid var(--grey-primary);
    }

    @media (min-width: 992px) {
      border-bottom: 2px solid var(--grey-primary);
      padding: 2rem 0;
      width: 85vw;
    }

    :where(.navbar > *) {
      display: inline-flex;
      align-items: center;
    }

    .navbar-start {
      .title {
        font-size: 1.5rem;
        font-weight: 700;
        text-transform: lowercase;
        color: var(--white-primary);
        margin-left: 1rem;
        display: none;

        @media (min-width: 768px) {
          display: block;
        }

        @media (min-width: 992px) {
          margin-left: 0;
        }
      }

      .menu-icon {
        cursor: pointer;
        @media (min-width: 992px) {
          display: none;
        }
      }
    }

    .navbar-center {
      p {
        color: var(--white-primary);
        font-size: 1.5rem;
        font-weight: 700;
        text-transform: lowercase;

        @media (min-width: 768px) {
          display: none;
        }
      }

      .nav-link {
        color: var(--white-primary);
        text-transform: uppercase;
        letter-spacing: 1.5px;
        font-weight: 400;

        &:hover {
          color: var(--orange-primary);
        }
      }

      ul {
        display: none;

        @media (min-width: 992px) {
          display: flex;
          align-items: center;
          gap: 1.5rem;
        }
      }
    }

    .navbar-end {
      position: relative;
      .cart-icon {
        cursor: pointer;
      }

      .cart-badge {
        position: absolute;
        background-color: var(--orange-primary);
        color: var(--black-primary);
        font-size: 0.8rem;
        width: 17px;
        height: 17px;
        border-radius: 50%;
        transform: translate(50%, -50%);
        right: 0;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    }
  }
`

export default Wrapper
