import { NavLink } from 'react-router-dom'
import links from '../utils/Links'
import { useHomeContext } from '../pages/HomeLayout'

const NavLinks = () => {
  const { toggleSidebar } = useHomeContext()

  return (
    <div className='nav-link-container'>
      {links.map((link) => {
        const { id, url, text, icon } = link
        return (
          <li key={id}>
            <NavLink className='nav-link' to={url} onClick={toggleSidebar} end>
              <span className='icon'>{icon}</span>
              {text}
            </NavLink>
          </li>
        )
      })}
    </div>
  )
}
export default NavLinks
