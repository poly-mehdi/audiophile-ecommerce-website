import Wrapper from '../assets/wrappers/Footer'
import { FaSquareFacebook, FaXTwitter, FaInstagram, FaS } from 'react-icons/fa6'
import NavLinks from './NavLinks'

const Footer = () => {
  return (
    <Wrapper>
      <div className='footer'>
        <div className='footer-items'>
          <h3>audiophile</h3>
          <ul>
            <NavLinks />
          </ul>
          <p className='footer-description'>
            Audiophile is an all in one stop to fulfill your audio needs. We're
            a small team of music lovers and sound specialists who are devoted
            to helping you get the most out of personal audio. Come and visit
            our demo facility - we’re open 7 days a week.
          </p>
          <p className='copyright'>Copyright 2021. All Rights Reserved</p>
          <div className='footer-social-links'>
            <a href='https://www.facebook.com' target='_blank' rel='noreferrer'>
              <FaSquareFacebook fill='#FFF' size={24} className='icon' />
            </a>
            <a href='https://www.twitter.com' target='_blank' rel='noreferrer'>
              <FaXTwitter fill='#FFF' size={24} className='icon' />
            </a>
            <a
              href='https://www.instagram.com'
              target='_blank'
              rel='noreferrer'
            >
              <FaInstagram fill='#FFF' size={24} className='icon' />
            </a>
          </div>
        </div>
      </div>
    </Wrapper>
  )
}
export default Footer
