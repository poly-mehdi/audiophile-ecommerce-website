import Wrapper from '../assets/wrappers/SpeakersPage'
import { ShopMenu, About } from '../components'
import { useLoaderData, Link } from 'react-router-dom'
import customFetch from '../utils/customFetch'

export const loader = async () => {
  const response = await customFetch(`/products/category/speakers`)
  return { products: response.data.products }
}

const Speakers = () => {
  const { products } = useLoaderData()
  const categoryAll = products[0].category
  return (
    <Wrapper>
      <div className='speaker-title-container'>
        <div className='speaker-title'>
          <h2>{categoryAll}</h2>
        </div>
      </div>
      {products.map((product, index) => {
        const {
          name,
          image,
          new: isNew,
          description,
          category,
          _id: id,
        } = product
        const isSecondProduct = index === 1
        return (
          <div className='speakers' key={index}>
            <div
              className={`speaker ${isSecondProduct ? 'second-product' : ''}`}
            >
              <picture>
                <source media='(min-width: 992px)' srcSet={image.desktop} />
                <source media='(min-width: 768px)' srcSet={image.tablet} />
                <img src={image.mobile} alt='speaker-1' className='img' />
              </picture>
              <div className='speaker-text'>
                {isNew && <p className='overline'>new product</p>}
                <div className='speaker-text-title'>
                  <h3>{name}</h3>
                  <h3>{category}</h3>
                </div>
                <p className='speaker-description'>{description}</p>
                <Link to={`/speakers/${id}`} className='btn-speaker'>
                  see product
                </Link>
              </div>
            </div>
          </div>
        )
      })}
      <ShopMenu />
      <About />
    </Wrapper>
  )
}
export default Speakers
