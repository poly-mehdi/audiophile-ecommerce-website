import { createSlice } from '@reduxjs/toolkit'
import { toast } from 'react-toastify'

const defaultState = {
  cartItems: [],
  numItemsInCart: 0,
  cartTotal: 0,
  shipping: 50,
  tax: 0,
  orderTotal: 0,
  overview: { firstItem: {}, total: 0, amount: 0 },
}

const getCartFromLocalStorage = () => {
  return JSON.parse(localStorage.getItem('cart')) || defaultState
}

const cartSlice = createSlice({
  name: 'cart',
  initialState: getCartFromLocalStorage(),
  reducers: {
    addItem: (state, action) => {
      const { product } = action.payload

      const item = state.cartItems.find((i) => i.cartID === product.cartID)
      if (item) {
        item.amount += product.amount
      } else {
        state.cartItems.push(product)
      }

      state.numItemsInCart += product.amount
      state.cartTotal += product.price * product.amount
      cartSlice.caseReducers.calculateTotals(state)
      toast.success('Item added to cart')
    },
    clearCart: (state) => {
      const overviewHistory = state.overview
      localStorage.setItem('cart', JSON.stringify(defaultState))
      return { ...defaultState, overview: overviewHistory }
    },
    clearOverview: (state) => {
      localStorage.setItem('cart', JSON.stringify(defaultState))
      return defaultState
    },
    editItem: (state, action) => {
      const { cartID, amount } = action.payload
      const item = state.cartItems.find((i) => i.cartID === cartID)
      state.numItemsInCart += amount - item.amount
      state.cartTotal += item.price * (amount - item.amount)
      item.amount = amount
      cartSlice.caseReducers.calculateTotals(state)
    },
    editAddItem: (state, action) => {
      const { cartID, amount } = action.payload
      const item = state.cartItems.find((i) => i.cartID === cartID)
      state.numItemsInCart += 1
      state.cartTotal += item.price
      item.amount = amount + 1
      cartSlice.caseReducers.calculateTotals(state)
    },
    editSubtractItem: (state, action) => {
      const { cartID, amount } = action.payload
      const item = state.cartItems.find((i) => i.cartID === cartID)
      if (amount > 1) {
        state.numItemsInCart -= 1
        state.cartTotal -= item.price
        item.amount = amount - 1
        cartSlice.caseReducers.calculateTotals(state)
      }
      if (amount == 1) {
        state.cartItems = state.cartItems.filter((i) => i.cartID !== cartID)
        state.numItemsInCart -= 1
        state.cartTotal -= item.price
        cartSlice.caseReducers.calculateTotals(state)
      }
    },
    calculateTotals: (state) => {
      state.tax = 0.2 * state.cartTotal
      state.orderTotal = state.cartTotal + state.shipping
      state.overview.firstItem = state.cartItems[0] || {}
      state.overview.total = state.orderTotal
      state.overview.amount =
        state.numItemsInCart - state.overview.firstItem.amount
      localStorage.setItem('cart', JSON.stringify(state))
    },
  },
})

export const {
  addItem,
  removeItem,
  editItem,
  clearCart,
  clearOverview,
  editAddItem,
  editSubtractItem,
} = cartSlice.actions

export default cartSlice.reducer
