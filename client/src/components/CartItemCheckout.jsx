import { formatPrice } from '../utils/format'

const CartItemCheckout = ({ cartItem }) => {
  const { cartID, image, name, price, amount } = cartItem

  return (
    <div className='cart-item'>
      <div className='cart-item-col-1'>
        <img src={image.mobile} alt='product' className='img item-img' />
        <div className='cart-item-product'>
          <h6>{name}</h6>
          <p>{formatPrice(price)}</p>
        </div>
      </div>
      <div className='cart-item-amount'>
        <span>x{amount}</span>
      </div>
    </div>
  )
}
export default CartItemCheckout
