import { BsCart3, BsList } from 'react-icons/bs'
import Wrapper from '../assets/wrappers/Navbar'
import { NavLink, Link } from 'react-router-dom'
import NavLinks from './NavLinks'
import { useHomeContext } from '../pages/HomeLayout'
import { useSelector } from 'react-redux'
// import useClickOutside from './useClickOutside'
// import { useRef } from 'react'

const Navbar = () => {
  const { toggleSidebar, toggleCart, closeCart } = useHomeContext()
  const numItemsInCart = useSelector((state) => state.cartState.numItemsInCart)
  // const ref = useRef(null)

  // useClickOutside(ref, closeCart)
  return (
    <Wrapper>
      <div className='navbar'>
        <div className='navbar-start'>
          {/* MENU */}
          <BsList
            fill='#ffffff'
            size={22}
            className='menu-icon'
            onClick={toggleSidebar}
          />
          {/* TITLE */}
          <NavLink to='/' className='title'>
            audiophile
          </NavLink>
        </div>
        <div className='navbar-center'>
          {/* TITLE tablet/mobile */}
          <Link to='/' className='title'>
            <p>audiophile</p>
          </Link>
          {/* NAV LINKS desktop */}
          <ul>
            <NavLinks />
          </ul>
        </div>
        <div className='navbar-end'>
          <BsCart3
            fill='#ffffff'
            size={22}
            className='cart-icon'
            onClick={toggleCart}
          />
          <span className='cart-badge'>{numItemsInCart}</span>
        </div>
      </div>
    </Wrapper>
  )
}
export default Navbar
