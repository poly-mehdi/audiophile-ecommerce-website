import styled from 'styled-components'

const Wrapper = styled.aside`
  /* * {
    border: 1px solid red;
  } */

  .cart-container {
    position: fixed;
    top: calc(3.5rem + 1.5rem + 1px);
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.4);
    display: flex;
    justify-content: center;
    z-index: -1;
    opacity: 0;
    transition: var(--transition);

    @media (min-width: 768px) {
      justify-content: end;
      padding-right: calc(10vw / 2);
      top: calc(3.5rem + 3.5rem);
    }

    @media (min-width: 992px) {
      padding-right: calc(15vw / 2);
    }

    @media (min-width: 1250px) {
      padding-right: calc((100vw - var(--max-width)) / 2);
    }

    .content {
      background: var(--white-tertiary);
      margin-top: 2rem;
      width: 90vw;
      height: fit-content;
      border-radius: 10px;
      padding: 2.5rem 2rem;
      position: relative;
      display: flex;
      align-items: center;
      flex-direction: column;
      gap: 1.5rem;

      @media (min-width: 768px) {
        width: 50vw;
      }

      @media (min-width: 992px) {
        width: 40vw;
        max-width: 30rem;
      }

      .cart-information {
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100%;

        h4 {
          text-transform: uppercase;
        }

        .cart-information-end {
          display: flex;
          align-items: center;
          gap: 1rem;

          p {
            color: var(--grey-secondary);
            cursor: pointer;
            text-decoration: underline;
            text-transform: capitalize;
            font-size: 1.1rem;

            &:hover {
              color: var(--orange-primary);
            }
          }

          .close-icon {
            cursor: pointer;
          }
        }
      }

      .cart-items {
        display: flex;
        flex-direction: column;
        gap: 1rem;
        width: 100%;
        overflow: auto;
        max-height: 14rem;

        .cart-item {
          display: flex;
          align-items: center;
          justify-content: space-between;

          .cart-item-col-1 {
            display: flex;
            align-items: center;
            gap: 1rem;

            .item-img {
              border-radius: 5px;
              width: 60px;
            }

            .cart-item-product {
              display: flex;
              flex-direction: column;

              h6 {
                font-size: 1rem;
              }

              p {
                color: var(--grey-secondary);
                font-weight: bold;
              }
            }
          }
          .cart-item-amount {
            display: flex;
            align-items: center;
            background-color: var(--white-primary);
            gap: 1.5rem;
            padding: 0 1rem;
            height: 3rem;

            .amount-add {
              cursor: pointer;
              color: var(--grey-secondary);
            }
          }
        }
      }

      .amount {
        display: flex;
        align-items: center;
        justify-content: space-between;
        width: 100%;

        h6 {
          color: var(--grey-secondary);
          font-weight: 400;
        }

        p {
          font-weight: bold;
          font-size: 1.3rem;
        }
      }

      .btn-container {
        width: 100%;
        background: var(--orange-primary);
        padding: 1rem 0;
        text-align: center;
        color: var(--white-primary);
        text-transform: uppercase;
        cursor: pointer;
        transition: var(--transition);
        font-weight: bold;
        letter-spacing: 1px;

        &:hover {
          background: var(--orange-secondary);
        }
      }

      .empty-icon {
        fill: var(--orange-primary);
      }
    }
  }
  .show-cart {
    z-index: 99;
    opacity: 1;
  }
`
export default Wrapper
