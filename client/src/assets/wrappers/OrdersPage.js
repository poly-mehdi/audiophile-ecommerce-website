import styled from 'styled-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;

  /* * {
    border: 1px solid red;
  } */

  .orders-title-container {
    background-color: var(--black-tertiary);
    padding: 3rem 0;
    text-align: center;
    gap: 1rem;

    @media (min-width: 768px) {
      padding: 4rem 0;
    }

    @media (min-width: 992px) {
      padding: 5rem 0;
    }
    h2 {
      color: var(--white-primary);
    }
  }

  .orders-container {
    padding: 2rem 0;
    margin-bottom: 2rem;
    display: flex;
    flex-direction: column;
    min-height: 25vh;

    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);

    @media (min-width: 992px) {
      width: 85vw;
    }
    .order-title {
      display: flex;
      align-items: center;
      border-bottom: 1px solid var(--grey-primary);
      padding: 1rem 0.5rem;

      .order-title-item {
        flex: 1;
        color: var(--grey-secondary);
        font-weight: 600;
        text-transform: capitalize;
      }

      .order-title-item:nth-child(1) {
        flex: 2;
      }
      .order-title-item:nth-child(2) {
        flex: 2;
      }
      .order-title-item:nth-child(3) {
        display: none;

        @media (min-width: 768px) {
          display: block;
        }
      }
      .order-title-item:last-child {
        flex: 2;
        display: none;

        @media (min-width: 768px) {
          display: block;
        }
      }
    }
    .orders-items {
      .order-item {
        display: flex;
        height: 4rem;
        align-items: center;
        padding: 0 0.5rem;

        .order-items-item {
          flex: 1;
        }
        .order-items-item:nth-child(1) {
          flex: 2;
        }
        .order-items-item:nth-child(2) {
          flex: 2;
        }
        .order-items-item:nth-child(3) {
          display: none;

          @media (min-width: 768px) {
            display: block;
          }
        }
        .order-items-item:last-child {
          flex: 2;
          display: none;

          @media (min-width: 768px) {
            display: block;
          }
        }
      }

      .order-item:nth-child(even) {
        background-color: var(--white-primary);
      }
    }
  }

  .pagination {
    display: flex;
    justify-content: center;
    padding: 2rem 0;
    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);

    @media (min-width: 768px) {
      justify-content: flex-end;
    }

    @media (min-width: 992px) {
      width: 85vw;
    }

    .pagination-btn {
      background-color: var(--black-tertiary);
      color: var(--white-primary);
      padding: 0.5rem 1rem;
      border-radius: 0.25rem;
      cursor: pointer;
      transition: var(--transition);

      &:hover {
        background-color: var(--grey-secondary);
      }
    }
  }

  .pagination-btn:nth-child(1) {
    border-bottom-left-radius: 5px;
    border-top-left-radius: 5px;
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
  }

  .pagination-btn:nth-child(2) {
    border-radius: 0;
    text-transform: capitalize;
  }

  .pagination-btn:nth-child(3) {
    border-bottom-right-radius: 5px;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
  }

  .orders-container-empty {
    padding: 2rem 0;
    text-align: center;
    min-height: 25vh;
    display: grid;
    place-items: center;
    p {
      color: var(--grey-secondary);
      font-weight: 600;
    }
  }
`

export default Wrapper
