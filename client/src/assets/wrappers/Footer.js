import styled from 'styled-components'

const Wrapper = styled.footer`
  background-color: var(--black-tertiary);
  padding: 2rem 0rem;
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 50%;
    transform: translateX(-50%);
    width: 100px;
    height: 0;
    border-top: 4px solid var(--orange-primary);
  }

  @media (min-width: 768px) {
    &::before {
      left: calc((10vw + 100px) / 2);
    }
  }

  @media (min-width: 992px) {
    padding: 4rem 0rem;
  }

  @media (min-width: 1250px) {
    &::before {
      left: calc((100vw - var(--max-width)) / 2 + 100px / 2);
    }
  }

  .footer {
    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);

    .footer-items {
      display: grid;
      text-align: center;
      gap: 1.5rem;
      grid-template-areas:
        'a'
        'b'
        'c'
        'd'
        'e';

      @media (min-width: 768px) {
        text-align: left;
        gap: 2rem;

        grid-template-areas:
          'a a'
          'b b'
          'c c'
          'd e';
      }

      h3 {
        color: var(--white-primary);
        font-size: 1.75rem;
        text-transform: lowercase;
        font-weight: 700;
        grid-area: a;
      }

      ul {
        display: flex;
        flex-direction: column;
        gap: 1rem;
        grid-area: b;

        @media (min-width: 768px) {
          flex-direction: row;
          gap: 2rem;
        }

        .nav-link {
          color: var(--white-primary);
          text-transform: uppercase;

          &:hover {
            color: var(--orange-primary);
          }
        }
      }

      .footer-description {
        color: var(--grey-secondary);
        margin-bottom: 2rem;
        grid-area: c;

        @media (min-width: 768px) {
          text-align: justify;
          text-justify: inter-word;
        }
      }

      .copyright {
        color: var(--grey-secondary);
        font-weight: bold;
        grid-area: d;
      }

      .footer-social-links {
        display: flex;
        justify-content: center;
        gap: 1rem;
        grid-area: e;

        @media (min-width: 768px) {
          justify-content: end;
        }

        .icon {
          cursor: pointer;
          transition: var(--transition);

          &:hover {
            fill: var(--orange-primary);
          }
        }
      }
    }
  }
`

export default Wrapper
