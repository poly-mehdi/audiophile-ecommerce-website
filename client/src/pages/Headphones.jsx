import Wrapper from '../assets/wrappers/HeadphonesPage'
import { ShopMenu, About } from '../components'
import { useLoaderData, Link } from 'react-router-dom'
import customFetch from '../utils/customFetch'

export const loader = async () => {
  const response = await customFetch(`/products/category/headphones`)
  return { products: response.data.products }
}

const Headphones = () => {
  const { products } = useLoaderData()
  const categoryAll = products[0].category
  return (
    <Wrapper>
      <div className='headphone-title-container'>
        <div className='headphone-title'>
          <h2>{categoryAll}</h2>
        </div>
      </div>
      {products.map((product, index) => {
        const {
          name,
          image,
          new: isNew,
          description,
          category,
          _id: id,
        } = product
        const isSecondProduct = index === 1
        return (
          <div className='headphones' key={index}>
            <div
              className={`headphone ${isSecondProduct ? 'second-product' : ''}`}
            >
              <picture>
                <source media='(min-width: 992px)' srcSet={image.desktop} />
                <source media='(min-width: 768px)' srcSet={image.tablet} />
                <img src={image.mobile} alt='headphone-1' className='img' />
              </picture>
              <div className='headphone-text'>
                {isNew && <p className='overline'>new product</p>}
                <div className='headphone-text-title'>
                  <h3>{name}</h3>
                  <h3>{category}</h3>
                </div>
                <p className='headphone-description'>{description}</p>
                <Link to={`/headphones/${id}`} className='btn-headphone'>
                  see product
                </Link>
              </div>
            </div>
          </div>
        )
      })}
      <ShopMenu />
      <About />
    </Wrapper>
  )
}
export default Headphones
