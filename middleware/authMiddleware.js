import {
  UnauthenticatedError,
  BadRequestError,
} from '../errors/customErrors.js'
import { verifyJWT } from '../utils/tokenUtils.js'

export const authenticateUser = (req, res, next) => {
  const { token } = req.cookies
  if (!token) throw new UnauthenticatedError('authentication invalid token')

  try {
    const { userId, role } = verifyJWT(token)
    const testUser = userId === '66201bb7f37ee087e33ead0b'
    req.user = { userId, role, testUser }
    next()
  } catch (error) {
    throw new UnauthenticatedError('authentication invalid error')
  }
}

export const authorizePermissions = (...roles) => {
  return (req, res, next) => {
    console.log(req.user.role, roles)
    if (!roles.includes(req.user.role)) {
      throw new UnauthenticatedError('Unauthorized access to this route')
    }
    next()
  }
}

export const softAuthenticateUser = (req, res, next) => {
  const { token } = req.cookies
  if (!token) {
    req.user = null
    next()
    return
  } else {
    try {
      const { userId, role } = verifyJWT(token)
      req.user = { userId, role }
      next()
    } catch (error) {
      throw new UnauthenticatedError('authentication invalid error')
    }
  }
}

export const checkForTestUser = (req, res, next) => {
  if (req.user.testUser) throw new BadRequestError('Demo User. Read Only')
  next()
}
