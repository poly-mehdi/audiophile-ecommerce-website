# Frontend Mentor - Audiophile e-commerce website solution

This is a solution to the [Audiophile e-commerce website challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/audiophile-ecommerce-website-C8cuSd_wx). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Add/Remove products from the cart
- Edit product quantities in the cart
- Fill in all fields in the checkout
- Receive form validations if fields are missed or incorrect during checkout
- See correct checkout totals depending on the products in the cart
  - Shipping always adds $50 to the order
  - VAT is calculated as 20% of the product total, excluding shipping
- See an order confirmation modal after checking out with an order summary
- **Bonus**: Keep track of what's in the cart, even after refreshing the browser (`localStorage` could be used for this if you're not building out a full-stack app)

### Screenshot

![](./design/audiophile_screenshot.png)

### Links

- Solution URL: [audiophile-website](https://audiophile-ecommerce-website-2if7.onrender.com/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Next.js](https://nextjs.org/) - React framework
- [Styled Components](https://styled-components.com/) - For styles
- [Redux](https://redux.js.org/) - State management for JavaScript applications
- [MongoDB](https://www.mongodb.com/) - NoSQL database
- [Express](https://expressjs.com/) - Web framework for Node.js
- [Node.js](https://nodejs.org/) - JavaScript runtime environment
- [Mongoose](https://mongoosejs.com/) - Object Data Modeling (ODM) for MongoDB

### What I learned

- How to effectively utilize Redux for state management in JavaScript applications.
- Utilizing Styled Components for styling in React applications, enhancing modularity and maintainability.
- Integrating the frontend with a Node.js server, understanding the full stack development process.

### Continued development

- Implementation of React Query to enhance data fetching and caching in the application.

## Author

- Website - [Portfolio](https://mehdisehad.dev)
- Frontend Mentor - [@polymehdi](https://www.frontendmentor.io/profile/poly-mehdi)
