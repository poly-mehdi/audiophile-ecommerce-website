import styled from 'styled-components'

const Wrapper = styled.section`
  .home {
    display: grid;
    grid-template-columns: 1fr;
  }

  .home-page {
    padding-bottom: 2rem;
  }

  .checkout-page {
    background-color: var(--white-primary);
  }
`

export default Wrapper
