import React from 'react'

export const formatPrice = (price) => {
  const dollarsAmount = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    maximumFractionDigits: 0,
  }).format(price)
  return dollarsAmount
}

export const formatTextForDisplay = (text) => {
  return text.split('\n').map((paragraph, index) => (
    <React.Fragment key={index}>
      {paragraph}
      <br />
    </React.Fragment>
  ))
}

export const formatDate = (dateString) => {
  const options = { year: 'numeric', month: 'short', day: 'numeric' }
  const formattedDate = new Date(dateString).toLocaleDateString(
    'en-US',
    options
  )
  return formattedDate
}
