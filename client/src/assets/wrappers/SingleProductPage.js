import styled from 'styled-components'

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  gap: 2rem;
  padding: 2rem 0;
  width: 90vw;
  margin: 0 auto;
  max-width: var(--max-width);

  @media (min-width: 992px) {
    width: 85vw;
  }

  .navigation {
    display: flex;
    align-items: center;
    gap: 1rem;

    ul {
      display: flex;
      align-items: center;
      gap: 0.5rem;
      list-style: none;

      .navigation-link:nth-child(odd) {
        color: var(--grey-secondary);
        text-transform: capitalize;
        cursor: pointer;

        &:hover {
          color: var(--orange-primary);
        }
      }

      .navigation-link:nth-child(even) {
        color: var(--grey-secondary);
        font-size: 1.1rem;
      }
    }
  }

  .product-header {
    display: flex;
    flex-direction: column;
    gap: 2rem;

    @media (min-width: 768px) {
      flex-direction: row;
      align-items: center;
    }

    @media (min-width: 992px) {
      display: grid;
      grid-template-columns: 1fr 1fr;
      gap: 2rem;
      align-items: center;
    }

    .img-product {
      border-radius: 10px;
      width: 100%;

      @media (min-width: 768px) {
        width: 18rem;
      }
      @media (min-width: 992px) {
        width: 30rem;
      }
    }

    .product-text {
      display: flex;
      flex-direction: column;
      gap: 1.5rem;
      margin-bottom: 2rem;

      .cart {
        display: flex;
        align-items: center;
        gap: 1.5rem;
        height: 3rem;

        .amount {
          display: flex;
          align-items: center;
          justify-content: center;
          background-color: var(--white-primary);
          gap: 2rem;
          padding: 0 1rem;
          height: 3rem;
          width: 8rem;

          .amount-add {
            cursor: pointer;
            color: var(--grey-secondary);
          }
        }

        .btn-product {
          background-color: var(--orange-primary);
          color: var(--white-secondary);
          text-transform: uppercase;
          display: inline-block;
          padding: 0.5rem 2rem;
          font-size: 15px;
          height: 3rem;

          &:hover {
            background-color: var(--orange-secondary);
          }
        }
      }
    }
  }

  .product-information {
    display: flex;
    flex-direction: column;
    gap: 2rem;

    @media (min-width: 768px) {
      flex-direction: row;
    }

    .features {
      display: flex;
      flex-direction: column;
      gap: 2rem;

      @media (min-width: 768px) {
        width: 65%;
      }
    }

    .box {
      display: flex;
      flex-direction: column;
      gap: 2rem;

      .box-items {
        display: flex;
        flex-direction: column;
        gap: 0.75rem;

        .box-item {
          display: flex;
          gap: 1rem;

          .quantity {
            color: var(--orange-primary);
            font-weight: 700;
            width: 1.5rem;
          }

          .item-name {
            color: var(--grey-secondary);
          }
        }
      }
    }
  }

  .gallery {
    display: flex;
    flex-direction: column;
    gap: 1rem;

    @media (min-width: 768px) {
      flex-direction: row;
      gap: 1rem;
      align-items: center;
    }

    img {
      border-radius: 10px;
    }

    .gallery-col-1 {
      display: flex;
      flex-direction: column;
      gap: 1.25rem;
    }
  }

  .others {
    display: flex;
    flex-direction: column;
    gap: 1rem;
    text-align: center;

    .others-container {
      @media (min-width: 768px) {
        display: flex;
        gap: 0.5rem;
        justify-content: center;
      }

      @media (min-width: 992px) {
        gap: 1rem;
      }

      .other {
        display: flex;
        flex-direction: column;
        gap: 1rem;
        margin-bottom: 2rem;

        .other-img {
          border-radius: 10px;
        }

        .other-text {
          display: flex;
          flex-direction: column;
          gap: 1rem;

          .btn-other {
            background-color: var(--orange-primary);
            color: var(--white-secondary);
            text-transform: uppercase;
            display: inline-block;
            padding: 0.5rem 2rem;
            font-size: 15px;
            width: 12rem;
            margin: 0 auto;

            @media (min-width: 768px) {
              width: 10rem;
              font-size: 13px;
            }

            &:hover {
              background-color: var(--orange-secondary);
            }
          }
        }
      }
    }
  }
  .shop-container {
    width: 90vw;
    margin: 0 auto;
    max-width: var(--max-width);
    padding: 2rem 0;
    margin-top: 6rem;

    @media (min-width: 992px) {
      width: 85vw;
    }

    .shop-items {
      display: flex;
      flex-direction: column;
      text-align: center;
      gap: 6rem;

      @media (min-width: 768px) {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        gap: 5px;
      }

      .shop-item {
        background-color: var(--white-primary);
        border-radius: 1rem;
        height: 10rem;
        position: relative;
        display: flex;
        align-items: end;
        justify-content: center;

        @media (min-width: 768px) {
          width: calc(90vw / 3 - 5px * 2);
        }

        @media (min-width: 992px) {
          width: calc(85vw / 3 - 5px * 2);
        }

        @media (min-width: 1250px) {
          width: calc(1120px / 3 - 5px * 2);
        }

        .shop-image {
          width: 11rem;
          margin: 0 auto;
          position: absolute;
          left: 50%;
          transform: translate(-50%, -40%);
        }

        .shop-item-text {
          display: flex;
          flex-direction: column;
          align-items: center;
          gap: 0.5rem;
          padding-bottom: 1rem;

          h6 {
            text-transform: capitalize;
          }

          .btn-container {
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;

            .btn-shop {
              background-color: transparent;
              color: var(--grey-secondary);
              text-transform: uppercase;
              display: inline-block;
              padding: 0.5rem 0.5rem;
              font-size: 13px;

              &:hover {
                color: var(--orange-primary);
              }
            }
          }
        }
      }
    }
  }
`

export default Wrapper
