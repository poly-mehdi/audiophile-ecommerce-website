import { Link, useNavigate } from 'react-router-dom'
import Wrapper from '../assets/wrappers/Header'
import { useHomeContext } from '../pages/HomeLayout'
import { useDispatch } from 'react-redux'
import { clearCart } from '../features/cart/cartSlice'
import { useEffect, useState, useCallback } from 'react'

const Header = () => {
  const { user, logoutUser } = useHomeContext()
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const handleLogout = () => {
    dispatch(clearCart())
    logoutUser()
  }

  return (
    <Wrapper>
      <header>
        <div className='header-items'>
          {/* USER */}
          {/* LINKS */}
          {user ? (
            <div className='header-links-login'>
              <p>Hello, {user.name}</p>
              <button className='btn btn-logout' onClick={handleLogout}>
                Logout
              </button>
            </div>
          ) : (
            <div className='header-links'>
              <Link to='/login' className='header-link'>
                Sign in / Guest
              </Link>
              <Link to='/register' className='header-link'>
                Create an Account
              </Link>
            </div>
          )}
        </div>
      </header>
    </Wrapper>
  )
}
export default Header
